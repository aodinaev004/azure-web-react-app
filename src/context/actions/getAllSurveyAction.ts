import environments from '../../environments/enviroment';

const API_URL = environments.localhost.api_surveys_url;

export async function getAllSurvey(dispatch: any, payload: any) {
  const requestOptions = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${payload.token}`
    }
  };

  try {
    dispatch({ type: 'GET_All_SURVEY_REQUEST' });
    let response = await fetch(`${API_URL}`, requestOptions);
    let data = await response.json();

    if (response.status === 401) dispatch({ type: 'LOGOUT' });
    if (data && data.surveyDetails) {
      dispatch({ type: 'GET_All_SURVEY_SUCCESS', payload: { surveys: data.surveyDetails } });
    } else {
      dispatch({ type: 'GET_All_SURVEY_ERROR', error: data.message });
    }
  } catch (error) {
    dispatch({ type: 'GET_All_SURVEY_ERROR', error: error });
  }
}