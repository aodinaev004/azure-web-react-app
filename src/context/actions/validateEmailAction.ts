import environments from '../../environments/enviroment';

const API_URL = environments.localhost.api_auth_url;

export async function validateEmailUser(dispatch: any, payload: any) {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  };

  try {
    dispatch({ type: 'VALIDATE_EMAIL_REQUEST' });
    let response = await fetch(`${API_URL}/validateemail`, requestOptions);
    let data = await response.json();

    if (data && data.success === true) {
      dispatch({
        type: 'VALIDATE_EMAIL_SUCCESS',
        payload: {
          status: data.success, token: data.token
        }
      });
    } else {
      dispatch({ type: 'VALIDATE_EMAIL_ERROR', error: data.message });
    }
  } catch (error) {
    dispatch({ type: 'VALIDATE_EMAIL_ERROR', error: error });
  }
}