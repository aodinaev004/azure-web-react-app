import environments from '../../environments/enviroment';

const API_URL = environments.localhost.api_auth_url;

export async function changePassUser(dispatch: any, payload: any) {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  };

  try {
    dispatch({ type: 'CHANGE_PASS_REQUEST' });
    let response = await fetch(`${API_URL}/changepassword`, requestOptions);
    let data = await response.json();

    if (data && data.success === true) {
      dispatch({
        type: 'CHANGE_PASS_SUCCESS',
        payload: {
          status: data.success, message: data.message
        }
      });
    }
  } catch (error) {
    dispatch({ type: 'CHANGE_PASS_ERROR', error: error });
  }
}