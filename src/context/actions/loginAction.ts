import environments from '../../environments/enviroment';

const API_URL = environments.localhost.api_auth_url;

// -----------------------  SignIn  -------------------------------
export async function loginUser(dispatch: any, payload: any) {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  };

  try {
    dispatch({ type: 'LOGIN_REQUEST' });
    let response = await fetch(`${API_URL}/signin`, requestOptions);
    let data = await response.json();

    if (data && data.accessToken && !data.accountExpired) {

      const { username, email, customerType, mobileNumber, accessToken } = data;
      const user = { username, email, customerType, mobileNumber };
      dispatch({ type: 'LOGIN_SUCCESS', payload: { user, accessToken } });

    } else {
      dispatch({ type: 'LOGIN_ERROR', error: data.message });
      setTimeout(() => dispatch({ type: 'LOGIN_DEFAULT' }), 1500);
    }
  } catch (error) {
    dispatch({ type: 'LOGIN_ERROR', error: error });
  }
}

// -----------------------  Logout   -------------------------------
export async function logout(dispatch: any) {
  dispatch({ type: 'LOGOUT' });
}