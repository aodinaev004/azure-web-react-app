import environments from '../../environments/enviroment';
import { getNumberOrStarType } from '../../utils/converter';

const API_URL = environments.localhost.api_surveys_url;

export async function createSurvey(dispatch: any, payload: any, token: string) {
  //Send to api
  payload = getNumberOrStarType(payload)

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify(payload),
  };
  
  try {
    dispatch({ type: 'CREATE_SURVEY_REQUEST' });
    let response = await fetch(`${API_URL}`, requestOptions);
    let data = await response.json();

    if (response.status === 401) dispatch({ type: 'LOGOUT' });

    if (response.status === 200 && data) {
      //from Api
      data = getNumberOrStarType(data, true)
      if( payload.id !== undefined &&  payload.id !== null) {
        dispatch({ type: 'CREATE_SURVEY_SUCCESS', payload:{ status: false, updateStatus : true} } );
        dispatch({ type: 'GET_SURVEY_SUCCESS', payload: { surveys: data } });
      }else{
        dispatch({ type: 'CREATED_SURVEY_ID' , id : data.id })
        dispatch({ type: 'CREATE_SURVEY_SUCCESS', payload:{ status: true, updateStatus : false} });
        dispatch({ type: 'GET_SURVEY_SUCCESS', payload: { surveys: data } });
      }
      setTimeout(() => {
        dispatch({ type: 'CREATE_SURVEY_DEFAULT' })
      }, 1500);
    } else {
      dispatch({ type: 'CREATE_SURVEY_ERROR', error: data.message });
    }
  } catch (error) {
    dispatch({ type: 'CREATE_SURVEY_ERROR', error: error });
  }
}