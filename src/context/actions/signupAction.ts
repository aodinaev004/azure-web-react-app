import environments from '../../environments/enviroment';

const API_URL = environments.localhost.api_auth_url;

export async function signUpUser(dispatch: any, payload: any) {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  };

  try {
    dispatch({ type: 'SIGNUP_REQUEST' });

    let response = await fetch(`${API_URL}/signup`, requestOptions);
    let data = await response.json();

    if (data && !data.errorCode) {
      dispatch({ type: 'SIGNUP_SUCCESS', payload: { status: true } });
    } else {
      dispatch({ type: 'SIGNUP_ERROR', error: data.message });
    }

  } catch (error) {
    dispatch({ type: 'SIGNUP_ERROR', error: error });
  }
}
