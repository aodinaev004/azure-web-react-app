import environments from '../../environments/enviroment';
import { getNumberOrStarType } from '../../utils/converter';

const API_URL = environments.localhost.api_surveys_url;

export async function getSurvey(dispatch: any, payload: any) {
  const requestOptions: RequestInit = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${payload.token}`
    },
  };

  try {
    dispatch({ type: 'GET_SURVEY_REQUEST', payload: { activeId: payload.id } });
    let response = await fetch(`${API_URL}/${payload.id}`, requestOptions);
    let data = await response.json();
    //From Api
    data = getNumberOrStarType(data, true)
    if (response.status === 401) dispatch({ type: 'LOGOUT' });

    if (response.status === 200 && data) {
      dispatch({ type: 'GET_SURVEY_SUCCESS', payload: { surveys: data } });
    } else {
      dispatch({ type: 'GET_SURVEY_ERROR', error: data.message });
    }
  } catch (error) {
    dispatch({ type: 'GET_SURVEY_ERROR', error: error });
  }
}
