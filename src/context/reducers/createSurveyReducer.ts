
export const initialCreateSurveyState = {
  surveyId: null,
  status: false,
  updateStatus: false,
  loading: false,
  errorMessage: null
};

export const CreateSurveyReducer = (initialState: any, action: any) => {
  switch (action.type) {
    case "CREATE_SURVEY_REQUEST":
      return {
        ...initialState,
        loading: true,
        status: false,
        updateStatus: false,
      };
    case "CREATE_SURVEY_SUCCESS":
      return {
        ...initialState,
        loading: false,
        status: action.payload.status,
        updateStatus: action.payload.updateStatus,
      };
    case "CREATED_SURVEY_ID":
      return {
        ...initialState,
        status: false,
        updateStatus: false,
        surveyId: action.id,
      };
    case "CREATE_SURVEY_ERROR":
      return {
        ...initialState,
        loading: false,
        status: false
      };
    case "CREATE_SURVEY_DEFAULT":
      return {
        surveyId: null,
        status: false,
        updateStatus: false,
        loading: false,
        errorMessage: null
      };
    default:
      return { ...initialState };
  }
};