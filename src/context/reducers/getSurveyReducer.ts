
export const initialGetSurveyState = {
  surveyData: null,
  toDefault: false,
  activeId: null,
  status: false,
  loading: false,
  errorMessage: null
};

export const GetSurveyReducer = (initialState: any, action: any) => {
  switch (action.type) {
    case "GET_SURVEY_REQUEST":
      return {
        ...initialState,
        loading: true,
        status: false,
        activeId: action.payload.activeId
      };
    case "GET_SURVEY_SUCCESS":
      return {
        ...initialState,
        surveyData: action.payload.surveys,
        loading: false,
        status: true
      };
    case "GET_SURVEY_ERROR":
      return {
        ...initialState,
        loading: false,
        status: false
      };
    case "GET_SURVEY_DEFAULT":
      return {
        surveyData: null,
        activeId: null,
        toDefault: true,
        status: false,
        loading: false,
        errorMessage: null
      };
    default:
      return { ...initialState };
  }
};