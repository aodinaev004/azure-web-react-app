export const initialSignupState = {
  status: false,
  loading: false,
  errorMessage: null,
};

export const SignupReducer = (initialSignupState: any, action: any) => {
  switch (action.type) {
    case "SIGNUP_REQUEST":
      return {
        ...initialSignupState,
        loading: true
      };
    case "SIGNUP_SUCCESS":
      return {
        ...initialSignupState,
        status: action.payload.status,
        loading: false,
        errorMessage: null
      };
    case "SIGNUP_ERROR":
      return {
        ...initialSignupState,
        loading: false,
        errorMessage: action.error
      };
    case "SIGNUP_DEFAULT":
      return {
        status: false,
        loading: false,
        errorMessage: null,
      };
    default:
      return {...initialSignupState};
  }
};