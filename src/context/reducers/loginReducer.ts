import { setCookie, getCookie, deletCookie } from '../../utils/cookie';

let user = localStorage.getItem("currentUser")
  ? JSON.parse(localStorage.getItem("currentUser")!)
  : "";
let auth = localStorage.getItem("auth")
  ? JSON.parse(localStorage.getItem("auth")!)
  : "";
let token = getCookie('app_token') ? getCookie('app_token') : "";

export const initialLoginState = {
  userDetails: "" || user,
  token: "" || token,
  auth: false || auth,
  loading: false,
  errorMessage: null
};

export const LoginReducer = (initialLoginState: any, action: any) => {

  switch (action.type) {
    case "LOGIN_REQUEST":
      return {
        ...initialLoginState,
        loading: true
      };
    case "LOGIN_SUCCESS":
      setCookie('app_token', action.payload.accessToken, 1);
      localStorage.setItem('currentUser', JSON.stringify(action.payload.user));
      localStorage.setItem('auth', JSON.stringify(true));
      return {
        ...initialLoginState,
        userDetails: action.payload.user,
        token: action.payload.accessToken,
        loading: false,
        errorMessage: null,
        auth: true
      };
    case "LOGOUT":
      deletCookie('app_token');
      localStorage.removeItem('currentUser');
      localStorage.removeItem('auth');
      return {
        ...initialLoginState,
        user: "",
        token: "",
        auth: false
      };
    case "LOGIN_ERROR":
      return {
        ...initialLoginState,
        loading: false,
        errorMessage: action.error
      };
    case "LOGIN_DEFAULT":
      return {
        ...initialLoginState,
        loading: false,
        errorMessage: null,
      };
    default:
      return { ...initialLoginState };
  }
};