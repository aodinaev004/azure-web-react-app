import { useReducer, useCallback, useMemo } from "react";
import { initialLoginState, LoginReducer } from "./loginReducer";
import { initialSignupState, SignupReducer } from "./signupReducer";
import { initialValidateEmailState, ValidateEmailReducer } from "./validateEmailReducer";
import { initialChangePassState, ChangePassReducer } from "./changePassReducer";
import { initialLangState, LangReducer } from "./langReducer";
import { initialLivePreviewState, LivePreviewReducer } from "./livePreviewReducer";
import { initialGetAllSurveyState, GetAllSurveyReducer } from "./getAllSurveyReducer";
import { initialGetSurveyState, GetSurveyReducer } from "./getSurveyReducer";
import { initialCreateSurveyState, CreateSurveyReducer } from "./createSurveyReducer";

const combineDispatch = (...dispatches: any) => (action: any) => {
  dispatches.map((dispatch: any) => {
    return dispatch(action)
  })
};

const useAllReducers = () => {
  const [livePreview, dispatchLivePreview] = useReducer(LivePreviewReducer, initialLivePreviewState);
  const [language, dispatchLang] = useReducer(LangReducer, initialLangState);
  const [login, dispatchLogin] = useReducer(LoginReducer, initialLoginState);
  const [rigister, dispatchSignin] = useReducer(SignupReducer, initialSignupState);
  const [validateEmail, dispatchValidateEmail] = useReducer(ValidateEmailReducer, initialValidateEmailState);
  const [changePass, dispatchChangePass] = useReducer(ChangePassReducer, initialChangePassState);
  const [surveysAll, dispatchSurveysAll] = useReducer(GetAllSurveyReducer, initialGetAllSurveyState);
  const [surveys, dispatchSurveys] = useReducer(GetSurveyReducer, initialGetSurveyState);
  const [createSurvey, dispatchCreateSurveys] = useReducer(CreateSurveyReducer, initialCreateSurveyState);

  /*eslint-disable */
  const combinedDispatch = useCallback(
    combineDispatch(
      dispatchLogin, dispatchSignin, dispatchValidateEmail, dispatchChangePass,
      dispatchLang, dispatchSurveysAll, dispatchSurveys, dispatchLivePreview,
      dispatchCreateSurveys
    ),
    [
      dispatchLogin, dispatchSignin, dispatchValidateEmail, dispatchChangePass,
      dispatchLang, dispatchSurveysAll, dispatchSurveys, dispatchLivePreview,
      dispatchCreateSurveys
    ]
  );
  /*eslint-enable */
  const combinedState = useMemo(() => (
    {
      login, rigister, validateEmail, changePass, language,
      surveysAll, surveys, livePreview, createSurvey
    }
  ),
    [
      login, rigister, validateEmail, changePass, language,
      surveysAll, surveys, livePreview, createSurvey
    ]
  );

  return [
    combinedState,
    combinedDispatch
  ]
};

export default useAllReducers;
export const contextState = {
  initialLangState,
  initialSignupState,
  initialLoginState,
  initialValidateEmailState,
  initialChangePassState,
  initialGetAllSurveyState,
  initialGetSurveyState,
  initialLivePreviewState,
  initialCreateSurveyState
}