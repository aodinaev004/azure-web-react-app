
export const initialGetAllSurveyState = {
  surveyAllData: [],
  searchResult: [],
  status: false,
  loading: false,
  errorMessage: null
};

export const GetAllSurveyReducer = (initialGetAllSurveyState: any, action: any) => {
  switch (action.type) {
    case "GET_All_SURVEY_REQUEST":
      return {
        ...initialGetAllSurveyState,
        loading: true,
        status: false
      };
    case "GET_All_SURVEY_SUCCESS":
      return {
        ...initialGetAllSurveyState,
        surveyAllData: action.payload.surveys,
        searchResult: action.payload.surveys,
        loading: false,
        status: true,
      };
    case "GET_All_SURVEY_SEARCH_FIND":
      return {
        ...initialGetAllSurveyState,
        searchResult: action.payload.surveys,
        searchValue: action.payload.search,
      };
    case "GET_All_SURVEY_FILTER_FIND":
      return {
        ...initialGetAllSurveyState,
        searchResult: action.payload.surveys,
        filterValue: action.payload.filter,
      };
    case "GET_All_SURVEY_SEARCH_DEFAULT_LIST":
      return {
        ...initialGetAllSurveyState,
        searchResult: initialGetAllSurveyState.surveyAllData
      };
    case "GET_All_SURVEY_FILTER_DEFAULT_LIST":
      return {
        ...initialGetAllSurveyState,
        searchResult: initialGetAllSurveyState.surveyAllData
      };
    case "GET_All_SURVEY_ERROR":
      return {
        ...initialGetAllSurveyState,
        loading: false,
        status: false
      };
    case "GET_All_SURVEY_DEFAULT":
      return {
        surveyAllData: [],
        searchResult: [],
        status: false,
        loading: false,
        errorMessage: null
      };
    default:
      return { ...initialGetAllSurveyState };
  }
};