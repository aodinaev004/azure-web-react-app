export const initialLivePreviewState = {
    questionTitle: "",
    questionHint: "",
    isRequired: false,
    checkboxOptions: [],
    radioOptions: [],
    dropdownOptions: [],
    sliderDefaultValue: 0,
    sliderMin: 1,
    sliderMax: 10,
    sliderStep: 1,
    numberMin: 1,
    numberMax: 5,
    numberStep: 1,
    starMax: 5,
    starStep: 1,
    ratingType: 'Star',
};

export const LivePreviewReducer = (initialLivePreviewState: any, action: any) => {

    switch (action.type) {
        case "PREVIEW":
            return {
                ...initialLivePreviewState,
                questionTitle: action.payload.questionTitle,
                questionHint: action.payload.questionHint,
                isRequired: action.payload.isRequired,
            };
        case "PREVIEW_CHECKBOX_OPTIONS":
            return {
                ...initialLivePreviewState,
                checkboxOptions: action.payload.checkboxOptions,
            };
        case "PREVIEW_RADIO_OPTIONS":
            return {
                ...initialLivePreviewState,
                radioOptions: action.payload.radioOptions,
            };
        case "PREVIEW_DROPDOWN_OPTIONS":
            return {
                ...initialLivePreviewState,
                dropdownOptions: action.payload.dropdownOptions,
            };
        case "PREVIEW_SLIDER_PROPERTIES":
            return {
                ...initialLivePreviewState,
                sliderDefaultValue: action.payload.sliderDefaultValue,
                sliderMin: action.payload.sliderMin,
                sliderMax: action.payload.sliderMax,
                sliderStep: action.payload.sliderStep,
            };
        case "PREVIEW_NUMBER_PROPERTIES":
            return {
                ...initialLivePreviewState,
                numberMin: action.payload.numberMin,
                numberMax: action.payload.numberMax,
                numberStep: action.payload.numberStep,
            };
        case "PREVIEW_STAR_PROPERTIES":
            return {
                ...initialLivePreviewState,
                starMax: action.payload.starMax,
                starStep: action.payload.starStep,
            };
        case "PREVIEW_RATING_TYPE_PROPERTIES":
            return {
                ...initialLivePreviewState,
                ratingType: action.payload.ratingType,
            };
        default:
            return { ...initialLivePreviewState };
    }
};