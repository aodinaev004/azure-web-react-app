export const initialValidateEmailState = {
  status: false,
  loading: false,
  token: null,
  errorMessage: null,
};

export const ValidateEmailReducer = (initialValidateEmailState: any, action: any) => {
  switch (action.type) {
    case "VALIDATE_EMAIL_REQUEST":
      return {
        ...initialValidateEmailState,
        errorMessage: null,
        status: false,
        loading: true,
      };
    case "VALIDATE_EMAIL_SUCCESS":
      return {
        ...initialValidateEmailState,
        token: action.payload.token,
        status: action.payload.status,
        loading: false
      };
    case "VALIDATE_EMAIL_ERROR":
      return {
        ...initialValidateEmailState,
        loading: false,
        errorMessage: action.error
      };
    case "VALIDATE_EMAIL_DEFAULT":
      return {
        status: false,
        loading: false,
        token: null,
        errorMessage: null
      };
    default:
      return {...initialValidateEmailState};
  }
};