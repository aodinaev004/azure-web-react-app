export const initialChangePassState = {
  status: false,
  loading: false,
  errorMessage: null,
  succesMessage: null,
};

export const ChangePassReducer = (initialChangePassState: any, action: any) => {
  switch (action.type) {
    case "CHANGE_PASS_REQUEST":
      return {
        ...initialChangePassState,
        status: false,
        loading: true,
        errorMessage: null,
        succesMessage: null,
      };
    case "CHANGE_PASS_SUCCESS":
      return {
        ...initialChangePassState,
        status: action.payload.status,
        succesMessage: action.payload.message,
        loading: false
      };
    case "CHANGE_PASS_ERROR":
      return {
        ...initialChangePassState,
        loading: false,
        errorMessage: action.error
      };
    case "CHANGE_PASS_DEFAULT":
      return {
        status: false,
        loading: false,
        errorMessage: null,
        succesMessage: null,
      };
    default:
      return {...initialChangePassState};
  }
};