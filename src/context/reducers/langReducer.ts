
let defLang = localStorage.getItem("lang") || "en";

export const initialLangState = {
  lang: defLang
};

export const LangReducer = (initialLangState: any, action: any) => {

  switch (action.type) {
    case "LANG_CHANGE":
      localStorage.setItem("lang", action.payload.language)
      return {
        lang: action.payload.language
      };
    default:
      return {...initialLangState};
  }
};