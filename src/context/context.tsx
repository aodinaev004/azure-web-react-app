import React from 'react';
import { I18nextProvider } from 'react-i18next';
import i18n from '../config/localization/i18n';
import { createTheme, ThemeProvider } from "@material-ui/core/styles";
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';


import useAllReducers, { contextState } from './reducers/combineReducer'
import AppFooter from '../components/common/AppFooter';
import AppHeader from '../components/common/AppHeader';
import colors from "../config/colors";

const AuthStateContext = React.createContext<any>(contextState);
const AuthDispatchContext = React.createContext<any>(contextState);

const theme = createTheme({
  overrides: {
    MuiButton: {
      containedPrimary: {
        backgroundColor: colors.primary,
        '&:hover': {
          backgroundColor: "#4292dc"
        },
        textTransform: "initial",
        fontFamily: "Poppins"
      },
    },
  },
});

export function useAuthState() {
  const context = React.useContext(AuthStateContext);
  if (context === undefined) {
    throw new Error("useAuthState must be used within a AuthProvider");
  }
  return context;
}

export function useAuthDispatch() {
  const context = React.useContext(AuthDispatchContext);
  if (context === undefined) {
    throw new Error("useAuthDispatch must be used within a AuthProvider");
  }
  return context;
}



interface ChildType {
  children: React.ReactNode;
}

export const AuthProvider = ({ children }: ChildType) => {
  const [state, dispatch] = useAllReducers();

  return (
    <AuthStateContext.Provider value={state}>
      <AuthDispatchContext.Provider value={dispatch}>
        <I18nextProvider i18n={i18n}>
          <ThemeProvider theme={theme}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <AppHeader />
              {children}
              <AppFooter />
            </MuiPickersUtilsProvider>
          </ThemeProvider>
        </I18nextProvider>
      </AuthDispatchContext.Provider>
    </AuthStateContext.Provider>
  );
};
