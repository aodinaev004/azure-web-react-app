import React, { useState, useEffect, useCallback, useMemo } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { Grid, Paper } from '@material-ui/core';
import Typography from "@material-ui/core/Typography";
import { FixedSizeGrid as List } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';

import AppSuspenseLoading from '../../components/common/AppSuspenseLoading';
import { getAllSurvey, getSurvey, useAuthDispatch, useAuthState } from '../../context';
import AppEmployeeList from '../../components/dashboard/AppEmployeeList';
import AppSearch from '../../components/dashboard/AppSearch'
import AppSelect from '../../components/dashboard/AppSelect';
import AppSurveyTotal from '../../components/dashboard/AppSurveyTotal';
import { AllSurveyT } from '../../config/types';
import colors from '../../config/colors';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            display: "flex"
        },
        paper: {
            height: "calc(100vh - 140px)",
            overflow: 'auto',
            overflowX: "hidden"
        },
        '& .fixSpace':{
            height:'130px !important'
        },
        '@global': {
            '*::-webkit-scrollbar': {
                width: 6,
                height: 6
            },
            '*::-webkit-scrollbar-thumb': {
                borderRadius: 10,
                backgroundColor: colors.scroll
            }
        },
        grigTotal: {
            borderRadius: 5,
            position: 'relative',
            border: '2px solid ' + colors.primary,
            height: "80px",
            marginTop: 10,
            padding: 10
        },
        gridTotalLabel: {
            backgroundColor: '#fcfcfc',
            color: 'rgb(109, 171, 228)',
            position: 'absolute',
            top: '-11px',
            width: '110px'
        }
    }),
);

const AppEmployeeListMemo = React.memo(AppEmployeeList);

export default function Dashboard() {
    const { t } = useTranslation();
    const classes = useStyles();
    const dispatch = useAuthDispatch();
    const {
        surveysAll: { surveyAllData, searchResult, status, loading },
        surveys: {
            surveyData,
            activeId,
            loading: loadingSurvey,
            status: statusSurvey
        },
        login: { token }
    } = useAuthState();
    const [search, setSearch] = useState('');
    const [searchResState, setSearchResState] = useState(surveyAllData)

    const [selectState, setSelectState] = useState('');

    const options = [{
        value: "error",
        text: "dash_filter_Error"
    },
    {
        value: "draft",
        text: "dash_filter_Draft"
    },
    {
        value: "processing",
        text: "dash_filter_Processing"
    },
    {
        value: "success",
        text: "dash_filter_Success"
    }]

    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setSelectState(event.target.value as string);
    };

    const handleChangeSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(e.target.value);
    }

    //Function for searching and filtering by status
    const filterListBy = useCallback((type) => {
        const data = [...surveyAllData];
        let find_search = [];
        let find_selected = [];
        if (type === 'search') {
            find_search = data.filter((el: AllSurveyT) => el.name.toLowerCase().includes(search.toLowerCase()))
            if (selectState !== '') {
                find_selected = data.filter((el: AllSurveyT) => el.status.toLowerCase().includes(selectState) && el.name.toLowerCase().includes(search.toLowerCase()))
                setSearchResState(find_selected)
            } else {
                setSearchResState(find_search)
            }
        } else {
            find_selected = data.filter((el: AllSurveyT) => el.status.toLowerCase().includes(selectState.toLowerCase()))
            if (search !== '' && selectState) {
                find_search = data.filter((el: AllSurveyT) => {
                    return el.status.toLowerCase().includes(selectState) && el.name.toLowerCase().includes(search.toLowerCase())
                })
                setSearchResState(find_search)
            } else {
                setSearchResState(find_selected)
            }
        }
    }, [selectState, search, surveyAllData])

    const RowRenderer = ({ columnIndex, rowIndex, style }: { columnIndex:number, rowIndex: number, style: any }) => {
        return <AppEmployeeListMemo key={rowIndex}
            style={{
                ...style,
                height: style.height -5
            }}
            newClass={"GridItem"}
            title={searchResState[rowIndex].name}
            created={searchResState[rowIndex].createdAt}
            modified={searchResState[rowIndex].editedAt}
            questionCount={searchResState[rowIndex].numberOfQuestions}
            state={searchResState[rowIndex].status}
            surveyID={searchResState[rowIndex].id}
            active={searchResState[rowIndex].id === activeId}
        />
    };

    const SurveyDetailMemo = useMemo(() => (
        <>
            <Grid item xs={12} md={12}>
                <Typography
                    component="h6"
                    variant="h6"
                    style={{ fontSize: "1rem" }}
                >
                    {surveyData?.name}
                </Typography>
            </Grid>
            <Grid item xs={12} md={12}>
                <Typography
                    variant="body2"
                    color="textSecondary"
                    style={{ fontSize: "0.8rem" }}
                >
                    {surveyData?.description}
                </Typography>
            </Grid>
            <AppSurveyTotal />
        </>
    ), [surveyData])

    //If token exist then fetch employe lists 
    useEffect(() => {
        if (dispatch) {
            getAllSurvey(dispatch, { token })
        }
    }, [dispatch, token]);

    // If fetching  employe list from api end successfully then fill in searchResState wiht array of data
    useEffect(() => {
        if (status === true) {
            setSearchResState(surveyAllData)
        }
    }, [status, surveyAllData]);

    useEffect(() => {
        if (searchResult[0] && searchResult[0].id !== undefined) {
            getSurvey(dispatch, { token, id: searchResult[0].id });
        }
    }, [dispatch, searchResult, token]);

    /*
     * Search employe list  [search effected  if length of input > 2 ].
     * Fill in searchResState wiht result of finded data.
    */
    useEffect(() => {
        setSearchResState(searchResult)
        if (search) {
            filterListBy('search')
        }
    }, [search, searchResult, filterListBy])

    /*
     * Filter employe list by selected status  
     * Fill in searchResState wiht result of selected data.
    */
    useEffect(() => {
        setSearchResState(searchResult)
        if (selectState !== '') {
            filterListBy('filter')
        } else {
            filterListBy('search')
        }
    }, [selectState, searchResult, filterListBy])

    return (
        <>
            <Grid container spacing={3}>
                <Grid item xs={12} md={6} >
                    <div className={classes.formControl}>
                        <AppSearch placeholder={`${t('dash_SearchInput')}...`} search={search} handleChange={handleChangeSearch} />
                        <AppSelect
                            defaultValue="dash_filter_State"
                            options={options}
                            handleChange={handleChange}
                            value={selectState}
                        />
                    </div>
                    {(status === false && loading) && <AppSuspenseLoading />}
                    {(status === false && !loading) && <p style={{ textAlign: 'center' }}>{t('dash_req_NoResult')}</p>}
                    {
                        (status === true && !loading && searchResState) &&
                        <AutoSizer>
                            {({ width }) => (
                                <List
                                    style={{ overflowX: 'hidden' }}
                                    height={705}
                                    columnCount={1}
                                    width={width}
                                    className="Grid"
                                    columnWidth={width}
                                    rowCount={searchResState.length}
                                    rowHeight={134}
                                  
                                >
                                    {RowRenderer}
                                </List>
                            )}
                        </AutoSizer>
                    }
                </Grid>
                <Grid item xs={12} md={6} >
                    {(statusSurvey === false && loadingSurvey && status === true) && <AppSuspenseLoading />}
                    {(statusSurvey === false && !loadingSurvey && status === true) && <p style={{ textAlign: 'center' }}>{t('dash_req_NoResult')}</p>}
                    {
                        (statusSurvey === true && !loadingSurvey) &&
                        SurveyDetailMemo
                    }
                </Grid>
            </Grid>
        </>
    );
}