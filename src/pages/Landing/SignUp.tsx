import { useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { Grid } from '@material-ui/core';
import LockIcon from '@material-ui/icons/Lock';
import PersonIcon from '@material-ui/icons/Person';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import BusinessIcon from '@material-ui/icons/Business'
import PhoneIphoneIcon from '@material-ui/icons/PhoneIphone'
import RecentActorsOutlinedIcon from '@material-ui/icons/RecentActorsOutlined';
import { useTranslation, Trans } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import EmailIcon from '@material-ui/icons/Email';
import Alert from '@material-ui/lab/Alert';
import { useFormik } from 'formik';

import AppInput from '../../components/landing/AppInput';
import AppCheckbox from '../../components/landing/AppCheckbox';
import AppFormContainer from '../../components/landing/AppFormContainer';
import signUpImg from '../../assets/images/signup-image.jpg';
import AppFormButton from '../../components/landing/AppFormButton';
import { useAuthDispatch, useAuthState, signUpUser } from '../../context';
import '../../assets/css/style.css';
import AppSelect from '../../components/dashboard/AppSelect';
import { getValidationFields } from '../../utils/formik/FormikValidationFields';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        checkbox: {
            '& .MuiTypography-body1': {
                fontSize: "11px",
            }
        },
        selectIcon: { marginBottom: "-26px" }
    })
)

const selectOptions = [
    {
        value: "1",
        text: "form_CustomerType_Business_Owner"
    },
    {
        value: "2",
        text: "form_CustomerType_Self_Employed"
    },
    {
        value: "3",
        text: "form_CustomerType_Social_Media"
    },
    {
        value: "4",
        text: "form_CustomerType_Influencer"
    },
    {
        value: "5",
        text: "form_CustomerType_Student"
    }
]

const SignUp = () => {
    const { t } = useTranslation();
    let history = useHistory();
    const classes = useStyles();
    const dispatch = useAuthDispatch();
    const { rigister: { loading, errorMessage, status } } = useAuthState();

    const formik = useFormik({
        initialValues: {
            name: '',
            email: '',
            password: '',
            pesswordConfirm: '',
            company: '',
            mobile: '',
            customerType: '',
            agree: false
        },
        validationSchema: getValidationFields(["name", "email", "password", "pesswordConfirm", "company", "mobile", "customerType", "agree"]),
        onSubmit: (values) => {
            signUpUser(dispatch, {
                "username": values.name,
                "password": values.password,
                "companyName": values.company,
                "email": values.email,
                "customerType": values.customerType,
                "mobileNumber": values.mobile
            });
        },
    });

    useEffect(() => {
        if (status === true) {
            setTimeout(() => {
                dispatch({ type: 'SIGNUP_DEFAULT' })
                history.push('/signin')
            }, 1500);
        }
    }, [status, history, dispatch])

    return (
        <AppFormContainer>
            <Grid className="signup-content">
                <Grid className="signup-form">
                    {/* <h2 className="form-title">{t("form_SignUp")}</h2> */}
                    {
                        errorMessage &&
                        <Alert severity="error" style={{ marginBottom: 20 }}>
                            {`${errorMessage}`}
                        </Alert>
                    }
                    {
                        (status && !errorMessage) &&
                        <Alert severity="success" style={{ marginBottom: 20 }}>
                            Registration success
                        </Alert>
                    }
                    <form onSubmit={formik.handleSubmit}>
                        <Grid className="form-group">
                            <AppInput
                                placeholder={t("form_NamePlaceholder")}
                                name="name"
                                icon={<PersonIcon fontSize="small" />}
                                value={formik.values.name}
                                onChange={formik.handleChange}
                                error={formik.touched.name && Boolean(formik.errors.name)}
                                helperText={`${formik.touched.name && formik.errors.name}`}
                                fullWidth={true}
                            />
                        </Grid>
                        <Grid className="form-group">
                            <AppInput placeholder={t("form_EmailPlaceholder")} name="email" type="email" icon={<EmailIcon fontSize="small" />}
                                value={formik.values.email}
                                onChange={formik.handleChange}
                                error={formik.touched.email && Boolean(formik.errors.email)}
                                helperText={`${formik.touched.email && formik.errors.email}`}
                                fullWidth={true}
                            />
                        </Grid>
                        <Grid className="form-group">
                            <AppInput placeholder={t("form_CompanyPlaceholder")} name="company" icon={<BusinessIcon fontSize="small" />}
                                value={formik.values.company}
                                onChange={formik.handleChange}
                                error={formik.touched.company && Boolean(formik.errors.company)}
                                helperText={`${formik.touched.company && formik.errors.company}`}
                                fullWidth={true}
                            />
                        </Grid>
                        <Grid className="form-group">
                            <AppInput placeholder={t("form_MobilePlaceholder")} name="mobile" icon={<PhoneIphoneIcon fontSize="small" />}
                                value={formik.values.mobile}
                                onChange={formik.handleChange}
                                error={formik.touched.mobile && Boolean(formik.errors.mobile)}
                                helperText={`${formik.touched.mobile && formik.errors.mobile}`}
                                fullWidth={true}
                            />
                        </Grid>
                        <Grid className="form-group">
                            <AppInput placeholder={t("form_PasswordPlaceholder")} name="password" type="Password" icon={<LockIcon fontSize="small" />}
                                value={formik.values.password}
                                onChange={formik.handleChange}
                                error={formik.touched.password && Boolean(formik.errors.password)}
                                helperText={`${formik.touched.password && formik.errors.password}`}
                                fullWidth={true}
                            />
                        </Grid>
                        <Grid className="form-group">
                            <AppInput placeholder={t("form_RePasswordPlaceholder")} name="pesswordConfirm" type="Password" icon={<LockOpenIcon fontSize="small" />}
                                value={formik.values.pesswordConfirm}
                                onChange={formik.handleChange}
                                error={formik.touched.pesswordConfirm && Boolean(formik.errors.pesswordConfirm)}
                                helperText={`${formik.touched.pesswordConfirm && formik.errors.pesswordConfirm}`}
                                fullWidth={true}
                            />
                        </Grid>

                        <Grid className="form-group">
                            <AppSelect defaultValue={t("form_CustomerType_default")} options={selectOptions}
                                name="customerType"
                                fullWidth={true}
                                value={formik.values.customerType}
                                error={formik.touched.customerType && Boolean(formik.errors.customerType)}
                                helperText={`${formik.touched.customerType && formik.errors.customerType}`}
                                handleChange={formik.handleChange}
                                icon={<RecentActorsOutlinedIcon fontSize="small" />}
                            />
                        </Grid>

                        <Grid className="form-group">
                            <AppCheckbox className={"label-agree-term " + classes.checkbox} label={
                                <Trans i18nKey="form_acceptPolicy">
                                    <span>I agree all statements in <a href="/" className="term-service">Terms of service</a></span>
                                </Trans>
                            }
                                name="agree"
                                checked={formik.values.agree}
                                handleChange={formik.handleChange}
                                error={formik.touched.agree && Boolean(formik.errors.agree)}
                                helperText={`${formik.touched.agree && formik.errors.agree}`}
                            />
                        </Grid>
                        <Grid className="form-group form-button">
                            <AppFormButton classname="form-submit" text={t("form_register")} loading={loading} />
                        </Grid>
                    </form>
                </Grid>
                <Grid className="signup-image">
                    <figure><img src={signUpImg} alt="sing up" /></figure>
                    <Link to="/signin" className="signup-image-link">{t("form_member")}</Link>
                </Grid>
            </Grid>
        </AppFormContainer>
    );
}

export default SignUp;