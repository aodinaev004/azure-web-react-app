import { useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import LockIcon from '@material-ui/icons/Lock';
import Alert from '@material-ui/lab/Alert';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useAuthDispatch, useAuthState, loginUser } from '../../context';
import { useHistory } from 'react-router-dom';
import { useFormik } from 'formik';

import AppFormContainer from '../../components/landing/AppFormContainer';
import AppFormButton from '../../components/landing/AppFormButton';
import AppInput from '../../components/landing/AppInput';
import signInImg from '../../assets/images/signin-image.jpg';
import colors from '../../config/colors';
import '../../assets/css/style.css';
import { getValidationFields } from '../../utils/formik/FormikValidationFields';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            alignItems: "start",
        },
        forgot: {
            color: colors.primary,
            textDecoration: 'none',
            marginLeft: 'auto',
        },
        forgotGrid: {
            display: 'flex',
            marginBottom: 15
        },
        icon: {
            display: "flex",
            justifyContent: "center",
        }
    })
)

const SignIn = () => {
    const { t } = useTranslation();
    const classes = useStyles();
    const dispatch = useAuthDispatch();
    const { login: { loading, errorMessage, auth } } = useAuthState();
    let history = useHistory();

    const formik = useFormik({
        initialValues: {
            name: '',
            password: ''
        },
        validationSchema: getValidationFields(["name", "password"]),
        onSubmit: (values) => {
            loginUser(dispatch, {
                username: values.name,
                password: values.password
            })
        }
    })

    useEffect(() => {
        if (auth === true) {
            dispatch({ type: 'LOGIN_DEFAULT' })
            history.push('/dashboard')
        }
    }, [auth, dispatch, history])

    return (
        <AppFormContainer>
            <Grid className="signin-content">
                <Grid className="signin-image">
                    <figure>
                        <img src={signInImg} alt="sing up" />
                    </figure>
                    <Link to="/signup" className="signup-image-link">{t("form_CreateAccount")}</Link>
                </Grid>
                <Grid className="signin-form">
                    {/* <h2 className="form-title">{t("form_SignIn")}</h2> */}
                    {
                        errorMessage &&
                        <Alert severity="error" style={{ marginBottom: 20 }}>
                            {`${errorMessage}`}
                        </Alert>
                    }
                    {
                        auth &&
                        <Alert severity="success" style={{ marginBottom: 20 }}>
                            Login Success
                        </Alert>
                    }
                    <form onSubmit={formik.handleSubmit}>
                        <Grid className="form-group">
                            <AppInput
                                placeholder={t("form_NamePlaceholder")}
                                // type="email"
                                icon={<PersonIcon fontSize="small" />}
                                name="name"
                                value={formik.values.name}
                                onChange={formik.handleChange}
                                error={formik.touched.name && Boolean(formik.errors.name)}
                                helperText={`${formik.touched.name && formik.errors.name}`}
                                fullWidth={true}
                            />
                        </Grid>
                        <Grid className="form-group">
                            <AppInput
                                placeholder={t("form_PasswordPlaceholder")}
                                type="password"
                                icon={<LockIcon fontSize="small" />}
                                name="password"
                                value={formik.values.password}
                                onChange={formik.handleChange}
                                error={formik.touched.password && Boolean(formik.errors.password)}
                                helperText={`${formik.touched.password && formik.errors.password}`}
                                fullWidth={true}
                            />
                        </Grid>
                        <Grid className={classes.forgotGrid} >
                            <Link to="/forgot" className={classes.forgot} >{t("form_ForgotPass")}</Link>
                        </Grid>
                        <Grid className="form-group form-button">
                            <AppFormButton
                                text={t("form_LogIn")}
                                classname="form-submit"
                                loading={loading}
                            />
                        </Grid>
                    </form>
                    <Grid className="social-login" >
                        <span className="social-label">{t("form_LoginWith")}</span>
                        <ul className="socials">
                            <li><a href="/#"><i className={'display-flex-center zmdi zmdi-facebook ' + classes.icon}></i></a></li>
                            <li><a href="/#"><i className={'display-flex-center zmdi zmdi-twitter ' + classes.icon}></i></a></li>
                            <li><a href="/#"><i className={'display-flex-center zmdi zmdi-google ' + classes.icon}></i></a></li>
                        </ul>
                    </Grid>
                </Grid>
            </Grid>
        </AppFormContainer>
    );
}

export default SignIn;