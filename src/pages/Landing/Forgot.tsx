import { useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import Alert from '@material-ui/lab/Alert';
import LockIcon from '@material-ui/icons/Lock';
import { useTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import { useHistory } from 'react-router-dom';
import { useAuthDispatch, useAuthState, validateEmailUser, changePassUser } from '../../context';

import AppFormContainer from '../../components/landing/AppFormContainer';
import AppFormButton from '../../components/landing/AppFormButton';
import AppInput from '../../components/landing/AppInput';
import signInImg from '../../assets/images/signin-image.jpg';
import '../../assets/css/style.css';
import { getValidationFields } from '../../utils/formik/FormikValidationFields';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        checkbox: {
            width: "max-content",
        },
        icon: {
            display: "flex",
            justifyContent: "center",
        }
    })
)

const Forgot = () => {
    const classes = useStyles();
    const { t } = useTranslation();
    let history = useHistory();
    const dispatch = useAuthDispatch();
    const {
        validateEmail: { loading, errorMessage, status, token },
        changePass: {
            succesMessage,
            loading: loadingPass,
            errorMessage: errorMessagePass,
            status: statusPass,
        }
    } = useAuthState();

    const formik = useFormik({
        initialValues: {
            email: ''
        },
        validationSchema: getValidationFields(["email"]),
        onSubmit: (values) => {
            validateEmailUser(dispatch, {
                email: values.email
            })
        }
    })
    const formikPass = useFormik({
        initialValues: {
            password: ''
        },
        validationSchema: getValidationFields(["password"]),
        onSubmit: (values) => {
            changePassUser(dispatch, {
                token: token,
                password: values.password
            })
        }
    })

    useEffect(() => {
        if (statusPass === true) {
            setTimeout(() => {
                dispatch({ type: 'VALIDATE_EMAIL_DEFAULT' });
                dispatch({ type: 'CHANGE_PASS_DEFAULT' });
                history.push('/signin')
            }, 1500);
        }
    }, [statusPass, history, dispatch])

    return (
        <AppFormContainer>
            <Grid className="signin-content">
                <Grid className="signin-image">
                    <figure>
                        <img src={signInImg} alt="sing up" />
                    </figure>
                </Grid>
                <Grid className="signin-form">
                    {/* <h2 className="form-title">{t('form_Forgot')}</h2> */}
                    {
                        (errorMessage || errorMessagePass) &&
                        <Alert severity="error" style={{ marginBottom: 20 }}>
                            {`${errorMessage || errorMessagePass}`}
                        </Alert>
                    }
                    {
                        (status && !statusPass && !loadingPass && !errorMessagePass) &&
                        <Alert severity="info" style={{ marginBottom: 20 }}>
                            Please enter your new password
                        </Alert>
                    }
                    {
                        statusPass &&
                        <Alert severity="success" style={{ marginBottom: 20 }}>
                            {succesMessage}
                        </Alert>
                    }
                    <form onSubmit={(!status && !statusPass) ? formik.handleSubmit : formikPass.handleSubmit}>
                        <Grid className="form-group">

                            <AppInput placeholder={t('form_EmailPlaceholder')} icon={<AlternateEmailIcon fontSize="small" />}
                                name="email"
                                value={formik.values.email}
                                onChange={formik.handleChange}
                                error={formik.touched.email && Boolean(formik.errors.email)}
                                helperText={`${formik.touched.email && formik.errors.email}`}
                                disabled={status}
                                fullWidth={true}
                            />
                            {status && <AppInput
                                placeholder={t("form_PasswordPlaceholder")}
                                type="password"
                                disabled={loadingPass}
                                icon={<LockIcon fontSize="small" />}
                                name="password"
                                value={formikPass.values.password}
                                onChange={formikPass.handleChange}
                                error={formikPass.touched.password && Boolean(formikPass.errors.password)}
                                helperText={`${formikPass.touched.password && formikPass.errors.password}`}
                                style={{ marginTop: "20px" }}
                            />
                            }
                        </Grid>
                        <Grid className="form-group form-button">
                            {
                                !status ?
                                    <AppFormButton text={t('form_validateEmailBtn')} classname="form-submit" loading={loading} />
                                    :
                                    <AppFormButton text={t('form_changePassBtn')} classname="form-submit" loading={loadingPass} />
                            }
                        </Grid>
                    </form>
                    <Grid className="social-login">
                        <ul className="socials">
                            <li><a href="/#"><i className={'display-flex-center zmdi zmdi-facebook ' + classes.icon}></i></a></li>
                            <li><a href="/#"><i className={'display-flex-center zmdi zmdi-twitter ' + classes.icon}></i></a></li>
                            <li><a href="/#"><i className={'display-flex-center zmdi zmdi-google ' + classes.icon}></i></a></li>
                        </ul>
                    </Grid>
                </Grid>
            </Grid>
        </AppFormContainer>
    );
}

export default Forgot;