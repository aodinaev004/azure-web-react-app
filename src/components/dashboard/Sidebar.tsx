import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
    Avatar,
    Box,
    Button,
    Divider,
    Drawer,
    Hidden,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Toolbar,
    Typography
} from '@material-ui/core';
import { HelpOutline } from '@material-ui/icons';
import { useTranslation } from 'react-i18next';
import IconButton from '@material-ui/core/IconButton';
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import PermIdentityOutlinedIcon from '@material-ui/icons/PermIdentityOutlined';
import PeopleOutlinedIcon from '@material-ui/icons/PeopleOutlined';
import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import HelpOutlineOutlinedIcon from '@material-ui/icons/HelpOutlineOutlined';
import PowerSettingsNewOutlinedIcon from '@material-ui/icons/PowerSettingsNewOutlined';
import { useState } from 'react';
import clsx from 'clsx';

const user = {
    avatar: '/static/images/avatars/avatar_6.png',
    jobTitle: 'Senior Developer',
    name: 'Katarina Smith'
};

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        iconButton: {
            marginLeft: "auto",
        },
        toolbar: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
            padding: theme.spacing(0, 1),
            // necessary for content to be below app bar
            ...theme.mixins.toolbar,
        },
        secondMenu: {
            marginTop: "auto",
        },
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
            whiteSpace: 'nowrap',
            '& .MuiDrawer-paper': {
                top: "auto",
                height: "94vh",
                overflow: "hidden",
            }
        },
        drawerOpen: {
            width: drawerWidth,
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen,
            }),
        },
        drawerClose: {
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }),
            overflowX: 'hidden',
            width: theme.spacing(7) + 1,
            [theme.breakpoints.up('sm')]: {
                width: "60px",
            },
        },
    }),
);

const items = [
    {
        href: '/app/dashboard',
        icon: HelpOutline,
        title: 'Dashboard'
    },
    {
        href: '/app/customers',
        icon: HelpOutline,
        title: 'Customers'
    },
    {
        href: '/app/products',
        icon: HelpOutline,
        title: 'Products'
    },
    {
        href: '/app/account',
        icon: HelpOutline,
        title: 'Account'
    },
    {
        href: '/app/settings',
        icon: HelpOutline,
        title: 'Settings'
    },
    {
        href: '/login',
        icon: HelpOutline,
        title: 'Login'
    },
    {
        href: '/register',
        icon: HelpOutline,
        title: 'Register'
    },
    {
        href: '/404',
        icon: HelpOutline,
        title: 'Error'
    }
];

const DashboardSidebar = () => {
    const { t } = useTranslation();
    const classes = useStyles();
    const [open, setOpen] = useState(true);

    const handleDrawerOpen = () => {
        setOpen(!open);
    };

    const content = (
        <>
            <List>
                <ListItem button>
                    <ListItemIcon>{<PermIdentityOutlinedIcon />}</ListItemIcon>
                    <ListItemText primary={t("sideBar_account")} />
                </ListItem>
                <ListItem button>
                    <ListItemIcon>{<PeopleOutlinedIcon />}</ListItemIcon>
                    <ListItemText primary={t("sideBar_groups")} />
                </ListItem>
                <ListItem button>
                    <ListItemIcon>{<SettingsOutlinedIcon />}</ListItemIcon>
                    <ListItemText primary={t("sideBar_settings")} />
                </ListItem>
            </List>

            <List>
                <Divider />
                <ListItem button>
                    <ListItemIcon>{<InfoOutlinedIcon />}</ListItemIcon>
                    <ListItemText primary={t("sideBar_help")} />
                </ListItem>
                <Divider />
                <ListItem button>
                    <ListItemIcon>{<HelpOutlineOutlinedIcon />}</ListItemIcon>
                    <ListItemText primary={t("sideBar_faqs")} />
                </ListItem>
                <Divider />
                <ListItem button>
                    <ListItemIcon>{<PowerSettingsNewOutlinedIcon />}</ListItemIcon>
                    <ListItemText primary={t("sideBar_logout")} />
                </ListItem>
                <Divider />
                <Toolbar className={classes.toolbar}>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={classes.iconButton}
                    >
                        <DoubleArrowIcon style={{ transform: `rotate( ${open ? 180 : 0}deg)` }} color="primary" />
                    </IconButton>
                </Toolbar>
            </List>
        </>
    );

    return (
        <>
            {/* <Hidden lgUp> */}
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    }),
                }}
            >
                {content}
            </Drawer>
            {/* </Hidden> */}
        </>
    );
};

export default DashboardSidebar;