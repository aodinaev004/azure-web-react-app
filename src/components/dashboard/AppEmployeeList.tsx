import {
  Theme,
  createStyles,
  makeStyles
} from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import AppBadge from "./AppBadge";
import Grid from "@material-ui/core/Grid";

import AppEmployeeListAction from './AppEmployeeListAction';
import { getSurvey, useAuthDispatch, useAuthState } from '../../context';
import { formatDate } from '../../utils/converter';
import colors from '../../config/colors';
import { useMemo } from "react";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "block",
      // marginTop: 10,
      position: 'relative',
      overflow: 'unset',
      cursor: 'pointer',
      '&:hover': {
        backgroundColor: 'whitesmoke'
      }
    },
    active: {
      backgroundColor: colors.active,
    },
    details: {
      display: "flex",
      flexDirection: "column",
    },
    content: {
      flex: "1 0 auto",
    },
    cover: {
      width: 151,
    },
    controls: {
      display: "flex",
      alignItems: "center",
      paddingLeft: theme.spacing(1),
      paddingBottom: theme.spacing(1),
    },
    playIcon: {
      height: 38,
      width: 38,
    },
  })
);

interface EmployeeListType {
  title: string;
  state: string;
  created: string;
  modified: string;
  questionCount: number;
  surveyID: number;
  active?: boolean;
  style?: any;
  newClass?: any;
}

export default function AppEmployeeList({
  title,
  state,
  created,
  modified,
  questionCount,
  surveyID,
  active,
  style,
  newClass
}: EmployeeListType) {
  const classes = useStyles();
  const dispatch = useAuthDispatch();
  const { login: { token } } = useAuthState();

  created = formatDate(created);
  modified = formatDate(modified);

  const handleGetSurvey = () => {
    getSurvey(dispatch, { token, id: surveyID })
  }

  const AppEmployeeListActionMemo = useMemo(() => <AppEmployeeListAction surveyID={surveyID} />, [surveyID])
  return (
    <Card
      className={(active ? `${classes.root} ${classes.active}` : classes.root) +' ' + newClass }
      style={style}
    >
      <div className={classes.details} >
        <AppBadge
          onClick={handleGetSurvey}
          childElement={
            <Grid
              container
              direction="row"
              justifyContent="space-between"
              alignItems="center"
              spacing={3}
            >
              <Grid item xs={12} sm={9}>
                <CardContent className={classes.content}>
                  <Typography
                    component="h6"
                    variant="h6"
                    style={{ fontSize: "1rem" }}
                  >
                    {title}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    {`Created: ${created} | Modified: ${modified}`}
                  </Typography>
                </CardContent>
              </Grid>
              <Grid item xs={12} sm={3}>
                <CardContent className={classes.content}>
                  <Typography variant="body2" color="textSecondary">
                    {questionCount}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    Questions
                  </Typography>
                </CardContent>
              </Grid>
            </Grid>
          }
          state={state}
        />
        {AppEmployeeListActionMemo}
      </div>
    </Card>
  );
}

