import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { FormControl, FormHelperText, InputLabel, ListItemIcon } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import colors from '../../config/colors';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            // margin: theme.spacing(1),
            minWidth: 120,
        },
        select: {
            paddingLeft: theme.spacing(3) + 6,
        },
        selectIcon: {
            "& .MuiSvgIcon-root": {
                marginBottom: "-26px",
            },
            "& .MuiListItemIcon-root": {
                minWidth: "30px"
            },
            "& .MuiIcon-root": {
                marginBottom: "-26px"
            },
            "& .MuiInputBase-input": {
                display: 'flex'
            }
        }
    }),
);

interface SelectOptionsType {
    defaultValue?: string;
    label?: string;
    asterisk?: boolean;
    options: { value: string, text: string, icon?: React.ReactNode }[];
    name?: string;
    value?: string;
    error?: boolean;
    helperText?: string;
    handleChange?: (e: React.ChangeEvent<any>) => void;
    fullWidth?: boolean;
    icon?: React.ReactNode;
    className?: string;
}
const asteriskStyle = {
    color: 'red',
    fontSize: 22
}

export default function AppSelect({ label, asterisk, defaultValue, options, name, value, error, helperText, handleChange, fullWidth = false, icon, className }: SelectOptionsType) {
    const classes = useStyles();
    const { t } = useTranslation();

    return (
        <FormControl className={classes.selectIcon} fullWidth={fullWidth} error={error}>
            {label &&
                <InputLabel shrink htmlFor={`input-id-${label?.toLowerCase()}`} style={{ color: `${colors.primary}` }} >
                    {label}
                    {asterisk && <span style={asteriskStyle}>*</span>}
                </InputLabel>
            }
            {icon}
            <Select
                name={name}
                value={value}
                onChange={handleChange}
                displayEmpty
                className={`${className} ${icon ? classes.select : ''}`}
                inputProps={{ 'aria-label': 'Without label' }}
            >
                {defaultValue && <MenuItem value="">
                    {t(defaultValue)}
                </MenuItem>}
                {options.map((option, key) => {
                    return <MenuItem key={key} value={option.value}>
                        {option.icon && <ListItemIcon>
                            {option.icon}
                        </ListItemIcon>}
                        {t(option.text)}
                    </MenuItem>
                })}
            </Select>
            {error && <FormHelperText>{t(`${helperText}`)}</FormHelperText>}
        </FormControl>
    );
}
