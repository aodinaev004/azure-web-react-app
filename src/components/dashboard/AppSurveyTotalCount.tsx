
import { Grid } from '@material-ui/core';
import Typography from "@material-ui/core/Typography";
import EqualizerIcon from '@material-ui/icons/Equalizer';

import { SurveyTotalCountT } from '../../config/types';



const AppSurveyTotalCount = ({ title, total, icon }: SurveyTotalCountT ) => {
  
  return (
    <Grid item xs={12} sm={4} style={{display: 'flex', alignItems: 'center'}} >
        <EqualizerIcon fontSize="large" style={{marginRight: 10}}/>
        <Grid>
          <Typography  style={{ fontSize: "1rem", fontWeight: 'bold'}}>
              {title}
          </Typography>
          <Typography  style={{ fontSize: "1rem", fontWeight: 'bold'}}>
              {total}
          </Typography>
        </Grid>
    </Grid>
  );
}

export default AppSurveyTotalCount;