import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  Grid,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Tooltip,
  Typography,
} from "@material-ui/core";
import HelpOutlinedIcon from "@material-ui/icons/HelpOutlined";
import TextField from '@material-ui/core/TextField';
import { useEffect, useState } from "react";

import colors from "../../config/colors";
import { NewObject, QuestionType } from "../../config/types";
import AppCheckboxGroup from "../landing/AppCheckboxGroup";
import AppFormButton from "../landing/AppFormButton";
import AppInput from "../landing/AppInput";
import AppRadioButton from "../landing/AppRadioButton";
import Select from '@material-ui/core/Select';
import AppSelect from "./AppSelect";
import { makeStyles } from "@material-ui/styles";
import AppStarHoverRating from "./questiontypes/AppStarHoverRating";
import AppToggleButtonGroup from "../landing/AppToggleButtonGroup";
import Slider from '@material-ui/core/Slider';

const useStyles = makeStyles({
  dropdown: {
    minWidth: 200
  },
  questionGrid: {
    marginTop: 30
  }
})

const QuestionData = ({
  questionTitle,
  isRequired,
  questionHint,
  component,
}: {
  questionTitle: string;
  isRequired: boolean;
  questionHint: string;
  component: any;
}) => {
  return (
    <Grid container spacing={1}>
      {questionTitle && (
        <>
          <Grid item sm={12}>
            <Grid
              container
              spacing={2}
              direction="row"
              justifyContent="flex-start"
              alignItems="center"
            >
              <Grid item>
                <InputLabel>
                  {questionTitle}
                  {isRequired && (
                    <span style={{ color: "red", fontSize: 22 }}> *</span>
                  )}
                </InputLabel>
              </Grid>
              {questionHint && (
                <Grid item>
                  <Tooltip title={questionHint} placement="right">
                    <HelpOutlinedIcon
                      style={{ color: colors.tooltip }}
                      fontSize="small"
                    />
                  </Tooltip>
                </Grid>
              )}
            </Grid>
          </Grid>
          <Grid item sm={12}>
            {component}
          </Grid>
        </>
      )}
    </Grid>
  );
};

const AppPreviewVlaidationForm = ({
  questions,
  formErrors,
  formInitialValues,
  sliderValues,
  starValues,
  numberValues
}: {
  questions: QuestionType[];
  formErrors: NewObject;
  formInitialValues: NewObject;
  sliderValues: NewObject;
  starValues: NewObject;
  numberValues: NewObject;
}) => {
  const classes = useStyles()
  const [initialValues, setInitialValues] = useState(formInitialValues);
  const [sliderValue, setSliderValue] = useState<NewObject>(sliderValues);
  const [starValue, setValue] = useState<NewObject>(starValues);
  const [numberValue, setNumberValue] = useState<NewObject>(numberValues);

  const [render, setRender] = useState<JSX.Element>();

  const handleInputChange = (e: any) => {
    initialValues[e.target.name] = e.target.value;
  };

  const handleCheckboxChange = (e: any) => {
    let checkInitialValues = initialValues[e.target.name];
    checkInitialValues[e.target.id] = e.target.checked;

    initialValues[e.target.name] = checkInitialValues;
  };

  const getOptions = (questionOptions: { title: string }[]) => {
    let options = [] as string[];
    const questionOpt = questionOptions || [];
    questionOpt.map((opt) => options.push(opt.title));
    return options;
  };

  const handleNumberChange = (e: any, newValue: string, name: string) => {
      setNumberValue({
        ...numberValue,
        [name]: newValue
      });
      initialValues[name] = parseInt(newValue);
  };

  const handleStarChange = (e:React.ChangeEvent<any>, newValue: number | null) => {
    setValue({
      ...starValue,
      [e.target.name]: newValue
    });
    initialValues[e.target.name] = parseInt(e.target.value);
  }

  const handleSliderChange = (e:React.ChangeEvent<any>, newValue: number | number[], name:string) => {
    setSliderValue({
      ...sliderValue,
      [name]: newValue
    });
    initialValues[name] = newValue;
  }

  const handleValidateClick = (e: any) => {
    e.preventDefault();
    for (let key in initialValues) {
      if (key.startsWith("checkbox")) {
        let checkInitialValues = initialValues[key];
        formErrors[key] =
          Object.entries(checkInitialValues).filter(
            (check) => check[1] === true
          ).length === 0;
      } else if (key.startsWith("star_rating") || key.startsWith("rating") || key.startsWith("slider")) {
        formErrors[key] =
          parseInt(initialValues[key]) === 0;
      } else {
        formErrors[key] =
          initialValues[key] === null || initialValues[key] === "";
      }
    }

    setRender(<></>);
  };

  return (
    <Grid container>
      {questions.map((question, index) => {
        const field = `${question.type.toLowerCase()}${index}`;

        if (question.type.toLowerCase() === "radiobutton") {
          const radioOptions = getOptions(JSON.parse(question.body).choices);
          return (
            <>
              <Grid item sm={12} className={classes.questionGrid}>
                <QuestionData
                  questionTitle={`${index + 1}. ${question.title}`}
                  questionHint={question.description}
                  isRequired={question.required}
                  component={
                    <FormControl
                      fullWidth
                      error={formErrors[field]}
                      component="fieldset"
                    >
                      <RadioGroup name={field} row onChange={handleInputChange}>
                        {radioOptions &&
                          (radioOptions as string[]).map((option) => {
                            return (
                              <FormControlLabel
                                key={option}
                                value={option}
                                control={
                                  <Radio style={{ color: colors.primary }} />
                                }
                                label={option}
                              />
                            );
                          })}
                      </RadioGroup>
                      {formErrors[field] && question.required && (
                        <FormHelperText>
                          This question is required
                        </FormHelperText>
                      )}
                    </FormControl>
                  }
                />
              </Grid>
            </>
          );
        } else if (question.type.toLowerCase() === "checkbox") {
          const checkboxOptions = getOptions(JSON.parse(question.body).choices);
          return (
            <Grid item sm={12} className={classes.questionGrid}>
              <QuestionData
                questionTitle={`${index + 1}. ${question.title}`}
                questionHint={question.description}
                isRequired={question.required}
                component={
                  <FormControl fullWidth error={formErrors[field]}>
                    <FormGroup row>
                      {checkboxOptions &&
                        (checkboxOptions as string[]).map((option) => {
                          return (
                            <FormControlLabel
                              key={option}
                              control={
                                <Checkbox
                                  style={{ color: colors.primary }}
                                  id={option}
                                  name={field}
                                  onChange={handleCheckboxChange}
                                />
                              }
                              label={option}
                            />
                          );
                        })}
                    </FormGroup>
                    {formErrors[field] && question.required && (
                      <FormHelperText>This question is required</FormHelperText>
                    )}
                  </FormControl>
                }
              />
            </Grid>
          );
        } else if (question.type.toLowerCase() === "text") {
          return (
            <Grid item sm={12} className={classes.questionGrid}>
              <QuestionData
                questionTitle={`${index + 1}. ${question.title}`}
                questionHint={question.description}
                isRequired={question.required}
                component={
                  <FormControl fullWidth error={formErrors[field]}>
                    <AppInput
                      fullWidth
                      name={field}
                      onChange={handleInputChange}
                    />
                    {formErrors[field] && question.required && (
                      <FormHelperText>This question is required</FormHelperText>
                    )}
                  </FormControl>
                }
              />
            </Grid>
          );
        } else if (question.type.toLowerCase() === "comment_box") {
          return (
            <Grid item sm={12} className={classes.questionGrid}>
              <QuestionData
                questionTitle={`${index + 1}. ${question.title}`}
                questionHint={question.description}
                isRequired={question.required}
                component={
                  <FormControl fullWidth error={formErrors[field]}>
                    <TextField
                        name={field}
                        multiline
                        rows={3}
                        variant="outlined"
                        onChange={handleInputChange}
                    />
                    {formErrors[field] && question.required && (
                      <FormHelperText>This question is required</FormHelperText>
                    )}
                  </FormControl>
                }
              />
            </Grid>
          );
        } else if (question.type.toLowerCase() === "dropdown") {
          const selectOptions = getOptions(JSON.parse(question.body).choices);
          const options: { value: string, text: string, icon?: React.ReactNode }[] = [];
          selectOptions.map(option => {
            options.push({
              value: option,
              text: option 
            })
          })
          return (
            <Grid item sm={12} className={classes.questionGrid}>
              <QuestionData
                questionTitle={`${index + 1}. ${question.title}`}
                questionHint={question.description}
                isRequired={question.required}
                component={
                  <FormControl error={formErrors[field]}>
                    <AppSelect options={options}
                        name={field}
                        handleChange={handleInputChange}
                        className={classes.dropdown}
                    />
                    {formErrors[field] && question.required && (
                      <FormHelperText>This question is required</FormHelperText>
                    )}
                  </FormControl>
                }
              />
            </Grid>
          );
        } else if (question.type.toLowerCase() === "star_rating") {
          const starOptions = JSON.parse(question.body);
          return (
            <Grid item sm={12} className={classes.questionGrid}>
              <QuestionData
                questionTitle={`${index + 1}. ${question.title}`}
                questionHint={question.description}
                isRequired={question.required}
                component={
                  <FormControl error={formErrors[field]}>
                    
                    <AppStarHoverRating name={field} max={starOptions.max} step={starOptions.step} value={starValue[field]} handleChange={handleStarChange} />

                    {formErrors[field] && question.required && (
                      <FormHelperText>This question is required</FormHelperText>
                    )}
                  </FormControl>
                }
              />
            </Grid>
          );
        } else if (question.type.toLowerCase() === "rating") {
          const numberOptions = JSON.parse(question.body);
          return (
            <Grid item sm={12} className={classes.questionGrid}>
              <QuestionData
                questionTitle={`${index + 1}. ${question.title}`}
                questionHint={question.description}
                isRequired={question.required}
                component={
                  <FormControl error={formErrors[field]}>
                    <AppToggleButtonGroup
                      name={field}
                      min={numberOptions.min}
                      max={numberOptions.max}
                      step={numberOptions.step}
                      value={numberValue[field]}
                      handleChange={(e,newValue) => handleNumberChange(e,newValue,field)}
                    />
                    {formErrors[field] && question.required && (
                      <FormHelperText>This question is required</FormHelperText>
                    )}
                  </FormControl>
                }
              />
            </Grid>
          );
        } else if (question.type.toLowerCase() === "slider") {
          const {min,max,step} = JSON.parse(question.body);
          return (
            <Grid item sm={12} className={classes.questionGrid}>
              <QuestionData
                questionTitle={`${index + 1}. ${question.title}`}
                questionHint={question.description}
                isRequired={question.required}
                component={
                  <FormControl fullWidth error={formErrors[field]}>
                    <Grid container
                        direction="row"
                        justifyContent="flex-start"
                        alignItems="center"
                        spacing={3}>
                        <Grid item sm={11}>
                            <Slider
                                step={step}
                                marks
                                min={min}
                                max={max}
                                onChange={(e,newValue) => handleSliderChange(e,newValue,field)}
                                name={field}
                            />
                        </Grid>
                        <Grid item sm={1}>
                            <Typography>{sliderValue[field]}</Typography>
                        </Grid>
                    </Grid>
                    {formErrors[field] && question.required && (
                      <FormHelperText>This question is required</FormHelperText>
                    )}
                  </FormControl>
                }
              />
            </Grid>
          );
        }
      })}
      <Grid item sm={12} style={{marginTop: 15}}>
        <AppFormButton text="VALIDATE" onClick={handleValidateClick} />
      </Grid>
    </Grid>
  );
};

export default AppPreviewVlaidationForm;
