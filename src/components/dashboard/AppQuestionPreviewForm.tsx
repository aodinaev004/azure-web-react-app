export default function AppQuestionPreview({ previewQuestionsForm }: { previewQuestionsForm?: JSX.Element }) {
    return <div>{previewQuestionsForm}</div>
}