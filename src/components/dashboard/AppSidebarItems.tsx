import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import PermIdentityOutlinedIcon from '@material-ui/icons/PermIdentityOutlined';
import PeopleOutlinedIcon from '@material-ui/icons/PeopleOutlined';
import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import HelpOutlineOutlinedIcon from '@material-ui/icons/HelpOutlineOutlined';
import PowerSettingsNewOutlinedIcon from '@material-ui/icons/PowerSettingsNewOutlined';
import LanguageOutlinedIcon from '@material-ui/icons/LanguageOutlined';
import { useTranslation } from 'react-i18next';

import { useAuthDispatch, useAuthState } from '../../context';
import { Grid, Menu, MenuItem } from '@material-ui/core';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { useState } from 'react';
import colors from '../../config/colors';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        secondMenu: {
            marginTop: "auto",
        },
        toolbar: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
            padding: theme.spacing(0, 1),
            // necessary for content to be below app bar
            ...theme.mixins.toolbar,
        },
        iconButton: {
            marginLeft: "auto",
            marginRight: "auto",
        },
        nested: {
            paddingLeft: theme.spacing(7),
        },
        menu: {
            "& .MuiMenu-paper": {
                width: "125px"
            }
        }
    })
)

const AppSidebarItems = ({ open = false, handleOpen, hasArrowButton = true }: { open?: boolean, handleOpen?: () => void, hasArrowButton?: boolean }) => {
    const { t } = useTranslation();
    const classes = useStyles();
    const dispatch = useAuthDispatch();
    const { language: { lang } } = useAuthState();
    const { i18n } = useTranslation();
    const [langMenuEl, setLangMenuEl] = useState<null | HTMLElement>(null);

    const signoutClick = () => {
        dispatch({ type: 'LOGOUT' })
    };

    const handleLangChange = (language: string) => {
        setLangMenuEl(null);
        i18n.changeLanguage(language)
        dispatch({ type: "LANG_CHANGE", payload: { language } })
    }

    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setLangMenuEl(event.currentTarget);
    };

    const handleClose = () => {
        setLangMenuEl(null);
    };

    return <>
        <List>
            <ListItem button>
                <ListItemIcon>{<PermIdentityOutlinedIcon />}</ListItemIcon>
                <ListItemText primary={t("sideBar_account")} />
            </ListItem>
            <ListItem button>
                <ListItemIcon>{<PeopleOutlinedIcon />}</ListItemIcon>
                <ListItemText primary={t("sideBar_groups")} />
            </ListItem>
            <ListItem button>
                <ListItemIcon>{<SettingsOutlinedIcon />}</ListItemIcon>
                <ListItemText primary={t("sideBar_settings")} />
            </ListItem>
        </List>

        <List className={classes.secondMenu}>
            <Divider />
            <ListItem button>
                <ListItemIcon>{<InfoOutlinedIcon />}</ListItemIcon>
                <ListItemText primary={t("sideBar_help")} />
            </ListItem>
            <Divider />
            <ListItem button>
                <ListItemIcon>{<HelpOutlineOutlinedIcon />}</ListItemIcon>
                <ListItemText primary={t("sideBar_faqs")} />
            </ListItem>
            <Divider />
            <ListItem button onClick={signoutClick}>
                <ListItemIcon>{<PowerSettingsNewOutlinedIcon />}</ListItemIcon>
                <ListItemText primary={t("sideBar_logout")} />
            </ListItem>
            <Divider />
            <Grid container
                direction="row"
                justifyContent="space-between"
                alignItems="center">
                {open && <Grid item xs={9}>
                    <ListItem button onClick={handleMenu}>
                        <ListItemIcon>{<LanguageOutlinedIcon />}</ListItemIcon>
                        <ListItemText
                            primary={lang === "en" ? "English" : "Franch"}
                        />
                        {Boolean(langMenuEl) ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                </Grid>}
                {hasArrowButton && <Grid item xs={3}>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleOpen}
                        edge="start"
                        className={classes.iconButton}
                    >
                        <DoubleArrowIcon style={{ transform: `rotate( ${open ? 180 : 0}deg)`, color: colors.primary }} />
                    </IconButton>
                </Grid>}
            </Grid>
            <Menu
                id="simple-menu"
                anchorEl={langMenuEl}
                keepMounted
                open={Boolean(langMenuEl)}
                onClose={handleClose}
                className={classes.menu}
            >
                <MenuItem onClick={(e) => handleLangChange("en")}>English</MenuItem>
                <MenuItem onClick={(e) => handleLangChange("fr")}>Franch</MenuItem>
            </Menu>
        </List>
    </>
}

export default AppSidebarItems;