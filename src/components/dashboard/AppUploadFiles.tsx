import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import { Grid } from '@material-ui/core';
import colors from '../../config/colors';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            '& > *': {
                margin: theme.spacing(1),
                marginLeft: 0,
            },
        },
        input: {
            display: 'none',
        },
    }),
);

export default function UploadFiles({ previewImage, handleOnImageUpload }: { previewImage: string, handleOnImageUpload: any }) {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <input accept="image/*" className={classes.input} id="icon-button-file" type="file" onChange={handleOnImageUpload} />
            <Grid container>
                <Grid item sm={12}>
                    <label htmlFor="icon-button-file">
                        <IconButton style={{ color: colors.primary }} aria-label="upload picture" component="span">
                            <PhotoCamera />
                        </IconButton>
                    </label>
                </Grid>
                <Grid item sm={12}>
                    {
                        previewImage !== '' &&
                        <img src={previewImage} style={{ maxWidth: '100%' }} alt="survey upload" />
                    }
                </Grid>
            </Grid>
        </div >
    );
}