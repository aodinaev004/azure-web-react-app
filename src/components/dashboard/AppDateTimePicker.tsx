import React, { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { FormControl } from '@material-ui/core';
import { KeyboardDateTimePicker } from '@material-ui/pickers';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            width: 220,
        },
    }),
);

interface DateTimeProperties {
    value?: Date;
    label?: string;
    format?: string;
    className?: string;
    fullWidth?: boolean;
}

export default function AppDateTimePicker({ format = "MM/dd/yyyy HH:mm", label, className, fullWidth = false }: DateTimeProperties) {
    const classes = useStyles();
    const [selectedDate, handleDateChange] = useState<Date | null>(new Date());

    return (<>
        <FormControl fullWidth={fullWidth}>
            <KeyboardDateTimePicker
                disableToolbar
                variant="inline"
                format={format}
                margin="normal"
                id="date-picker-inline"
                label={label}
                value={selectedDate}
                onChange={handleDateChange}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                }}
                className={className}
            />
        </FormControl>
    </>
    );
}