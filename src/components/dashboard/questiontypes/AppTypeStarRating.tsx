import { FormControlLabel, FormGroup, Grid, InputLabel } from "@material-ui/core";
import Checkbox, { CheckboxProps } from '@material-ui/core/Checkbox';
import SaveIcon from '@material-ui/icons/Save';
import { useState } from "react";

import { useAuthState } from "../../../context";
import AppFormButton from "../../landing/AppFormButton";
import AppStarHoverRating from "./AppStarHoverRating"

export function LivePreview() {
    const { livePreview: { questionTitle } } = useAuthState();
    const [starValue, setValue] = useState<number | null>(0);

    const handleStarChange = (e:React.ChangeEvent<any>, newValue: number | null) => {
      setValue(newValue);
    }

    return <Grid container spacing={3}>
        <Grid item sm={12}>
            <InputLabel>{questionTitle}</InputLabel>
        </Grid>
        <Grid item sm={12}>
            {questionTitle && <AppStarHoverRating value={starValue} handleChange={handleStarChange} />}
        </Grid>
    </Grid>
}

const defaultLabels: { [index: string]: string } = {
    1: 'Useless',
    2: 'Poor',
    3: 'Ok',
    4: 'Good',
    5: 'Excellent',
};

export default function AppTypeTating() {
    return <Grid container spacing={3}>
        <Grid item sm={12} style={{ display: 'flex', alignItems: 'flex-end', justifyContent: 'flex-end' }}>
            <AppFormButton text="Save" startIcon={<SaveIcon />} />
        </Grid>
    </Grid>
}