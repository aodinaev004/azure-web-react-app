import { Grid, IconButton, InputLabel } from "@material-ui/core";
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { useEffect, useState } from "react";
import Tooltip from '@material-ui/core/Tooltip';
import HelpOutlinedIcon from '@material-ui/icons/HelpOutlined';

import AppInput from "../../landing/AppInput";
import AppOptionList from "./AppOptionList";
import { useAuthDispatch, useAuthState } from '../../../context';
import colors from "../../../config/colors";
import AppRadioButton from "../../landing/AppRadioButton";

export function LivePreview({ validation, questionTitle, questionHint, isRequired, optionsFromApi }
    : { validation?: boolean, questionTitle: string, questionHint: string, isRequired: boolean, optionsFromApi?: string[] }) {
    const { livePreview: { radioOptions } } = useAuthState();
    const [options, setOptions] = useState([] as string[]);
    const [value, setValue] = useState('');

    useEffect(() => {
        if (optionsFromApi) {
            setOptions(optionsFromApi)
        }
        else {
            setOptions(radioOptions)
        }
    }, [optionsFromApi, radioOptions])


    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setValue((event.target as HTMLInputElement).value);
    };

    return <Grid container spacing={1}>
        {questionTitle && <>
            <Grid item sm={12}>
                <Grid container spacing={2}
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="center">
                    <Grid item>
                        <InputLabel>{questionTitle}{isRequired && <span style={{color: "red", fontSize: 22}}>  *</span>}</InputLabel>
                    </Grid>
                    {questionHint && <Grid item>
                        <Tooltip title={questionHint} placement="right">
                            <HelpOutlinedIcon style={{ color: colors.tooltip }} fontSize="small" />
                        </Tooltip>
                    </Grid>}
                </Grid>
            </Grid>
            <Grid item sm={12}>
                {options.length > 0 && <AppRadioButton row={true} value={value} options={options} handleChange={handleChange} />}
            </Grid>
        </>}
    </Grid>
}

export default function AppTypeRadioButton() {
    const [newOption, setNewOption] = useState('');
    const { livePreview: { radioOptions } } = useAuthState();
    const dispatch = useAuthDispatch();

    const handleAddClick = () => {
        if (newOption !== '') {
            const options = radioOptions ? radioOptions.concat(newOption) : [newOption];
            dispatch({ type: "PREVIEW_RADIO_OPTIONS", payload: { radioOptions: options } })
            setNewOption('');
        }
    }

    const handleChange = (e: any) => {
        setNewOption(e.target.value);
    }

    const handleRemove = (option: string) => {
        const options = radioOptions.filter((d: string) => d !== option);
        dispatch({ type: "PREVIEW_RADIO_OPTIONS", payload: { radioOptions: options } })
    }

    return <Grid container spacing={3}>
        <Grid item sm={12}>
            <InputLabel style={{ color: `${colors.primary}` }}>Properties Section</InputLabel>
        </Grid>
        <Grid item sm={12}>
            <AppInput placeholder="New option" onChange={handleChange} value={newOption} />
            <IconButton color="primary" aria-label="upload picture" component="span" onClick={handleAddClick}>
                <AddCircleIcon style={{ color: colors.primary }} />
            </IconButton>
        </Grid>
        <Grid item xs={12} sm={6} style={{
            overflow: "auto",
            height:300,
        }}>
            {radioOptions && (radioOptions as string[]).map(option => option && <AppOptionList text={option} handleRemove={() => handleRemove(option)} />)}
        </Grid>
    </Grid>
}