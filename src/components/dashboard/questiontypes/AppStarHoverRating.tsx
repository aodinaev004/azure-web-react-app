import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';

const defaultLabels: { [index: string]: string } = {
    0.5: 'Useless',
    1: 'Useless+',
    1.5: 'Poor',
    2: 'Poor+',
    2.5: 'Ok',
    3: 'Ok+',
    3.5: 'Good',
    4: 'Good+',
    4.5: 'Excellent',
    5: 'Excellent+',
};

interface StarRatingProperties {
    labels?: { [index: string]: string };
    max?: number;
    step?: number;
    name?:string;
    value: number | null;
    handleChange: (e:React.ChangeEvent<any>, newValue: number | null) => void;
}

const useStyles = makeStyles({
    root: {
        width: 200,
        display: 'flex',
        alignItems: 'center',
    },
});

export default function AppStarHoverRating({ labels = defaultLabels, max, step, name, handleChange, value }: StarRatingProperties) {
    const [hover, setHover] = React.useState(-1);
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Rating
                name={name}
                value={value}
                precision={step}
                max={max}
                onChange={(event, newValue) => handleChange(event, newValue)}
                onChangeActive={(event, newHover) => {
                    setHover(newHover);
                }}
                size="large"
                onVolumeChange={() => console.log(11)}
            />
        </div>
    );
}
