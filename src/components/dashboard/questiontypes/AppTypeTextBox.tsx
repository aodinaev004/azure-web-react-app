import { Grid, InputLabel } from "@material-ui/core";
import Tooltip from '@material-ui/core/Tooltip';
import HelpOutlinedIcon from '@material-ui/icons/HelpOutlined';

import colors from '../../../config/colors';
import { FormikType } from '../../../utils/formik/FormikValidationFields';
import AppInput from "../../landing/AppInput";

export function LivePreview({ fik, questionTitle, questionHint, isRequired }
    : { fik?: FormikType, questionTitle: string, questionHint: string, isRequired: boolean }) {

    return <Grid container spacing={1}>
        {questionTitle && <>
            <Grid item sm={12}>
                <Grid container spacing={2}
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="center">
                    <Grid item>
                        <InputLabel>{questionTitle}{isRequired && <span style={{color: "red", fontSize: 22}}>  *</span>}</InputLabel>
                    </Grid>
                    {questionHint && <Grid item>
                        <Tooltip title={questionHint} placement="right">
                            <HelpOutlinedIcon style={{ color: colors.tooltip }} fontSize="small" />
                        </Tooltip>
                    </Grid>}
                </Grid>
            </Grid>
            <Grid item sm={12}>
                <AppInput
                    name={fik?.name}
                    value={fik?.value}
                    onChange={fik?.onchange}
                    error={fik?.error}
                    helperText={fik?.helperText}
                    fullWidth={true} />
            </Grid>
        </>}
    </Grid>
}

export default function AppTypeTextBox() {
    return <></>
}