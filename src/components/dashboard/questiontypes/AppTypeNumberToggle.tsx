import { Grid, InputLabel } from "@material-ui/core";
import SaveIcon from '@material-ui/icons/Save';
import { useState } from "react";

import colors from "../../../config/colors";
import { useAuthDispatch, useAuthState } from "../../../context";
import AppFormButton from "../../landing/AppFormButton";
import AppInput from "../../landing/AppInput";
import AppToggleButtonGroup from "../../landing/AppToggleButtonGroup";

export function LivePreview() {
    const { livePreview: { questionTitle, numberMin, numberMax, numberStep } } = useAuthState();

    return <Grid container spacing={3}>
        <Grid item sm={12}>
            <InputLabel>{questionTitle}</InputLabel>
        </Grid>
        <Grid item sm={12}>
            {questionTitle && <AppToggleButtonGroup min={numberMin} max={numberMax} step={numberStep} />}
        </Grid>
    </Grid>
}

export default function AppTypeNumberToggle() {
    const { livePreview: { numberMin, numberMax, numberStep } } = useAuthState();
    const dispatch = useAuthDispatch();
    const [min, setMin] = useState(numberMin);
    const [max, setMax] = useState(numberMax);
    const [step, setStep] = useState(numberStep);

    const handleChange = (e: React.ChangeEvent<any>) => {
        let targetvalue = Number(e.target.value);
        if (e.target.value === '' || e.target.value === undefined) targetvalue = 0;
        let numberMin = Number(min);
        let numberMax = Number(max);
        let numberStep = Number(step);
        switch (e.target.name) {
            case 'min':
                numberMin = targetvalue;
                setMin(numberMin)
                if (numberMin >= numberMax) {
                    numberMax = numberMin + 1;
                    setMax(numberMax);
                }
                if (numberMin > numberStep) {
                    numberStep = numberMin;
                    setStep(numberStep);
                }
                break;
            case 'max':
                numberMax = targetvalue;
                setMax(numberMax)
                if (numberMax <= numberMin) {
                    numberMin = numberMax - 1;
                    setMin(numberMin);
                }
                if (numberMax < numberStep) {
                    numberStep = numberMax;
                    setStep(numberStep);
                }
                break;
            case 'step':
                numberStep = targetvalue === 0 ? 1 : targetvalue;
                numberStep = numberStep < numberMin ? numberMin : numberStep;
                numberStep = numberStep > numberMax ? numberMax : numberStep;
                setStep(numberStep)
                break;
        }
        dispatch({ type: "PREVIEW_NUMBER_PROPERTIES", payload: { numberMin, numberMax, numberStep } })
    }

    return <Grid container spacing={3}>
        <Grid item sm={12}>
            <InputLabel style={{ color: colors.primary }}>Properties Section</InputLabel>
        </Grid>
        <Grid item xs={12} sm={8}>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={3}>
                    <AppInput label="Min" type='number' placeholder="Min" name="min" onChange={handleChange} value={min} />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <AppInput label="Max" type='number' placeholder="Max" name="max" onChange={handleChange} value={max} />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <AppInput label="Step" type='number' placeholder="Step" name="step" onChange={handleChange} value={step} />
                </Grid>
            </Grid>
        </Grid>
        <Grid item sm={12} style={{ display: 'flex', alignItems: 'flex-end', justifyContent: 'flex-end' }}>
            <AppFormButton text="Save" startIcon={<SaveIcon />} />
        </Grid>
    </Grid>
}