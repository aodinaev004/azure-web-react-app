import { FormControl, FormControlLabel, FormGroup, FormHelperText, Grid, IconButton, InputLabel } from "@material-ui/core";
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { useState } from "react";
import Checkbox from '@material-ui/core/Checkbox';
import Tooltip from '@material-ui/core/Tooltip';
import HelpOutlinedIcon from '@material-ui/icons/HelpOutlined';

import AppInput from "../../landing/AppInput";
import AppOptionList from "./AppOptionList";
import { useAuthDispatch, useAuthState } from '../../../context';
import colors from "../../../config/colors";
import { FormikType, getValidationFields } from "../../../utils/formik/FormikValidationFields";

export const initialValues = {
    checkbox: ''
}

export const validationSchema = getValidationFields(["checkbox"]);

export function LivePreview({ fik, questionTitle, questionHint, isRequired, optionsFromApi, fullWidth }
    : { fik?: FormikType, questionTitle: string, questionHint: string, isRequired: boolean, optionsFromApi?: string[], fullWidth?: boolean }) {

    const { livePreview: { checkboxOptions } } = useAuthState();

    const getOptions = () => {
        if (optionsFromApi) {
            return optionsFromApi;
        } else {
            return checkboxOptions;
        }
    }

    return <Grid container spacing={1}>
        {questionTitle && <>
            <Grid item sm={12}>
                <Grid container spacing={2}
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="center">
                    <Grid item>
                        <InputLabel>{questionTitle}{isRequired && <span style={{ color: "red", fontSize: 22 }}>  *</span>}</InputLabel>
                    </Grid>
                    {questionHint && <Grid item>
                        <Tooltip title={questionHint} placement="right">
                            <HelpOutlinedIcon style={{ color: colors.tooltip }} fontSize="small" />
                        </Tooltip>
                    </Grid>}
                </Grid>
            </Grid>
            <Grid item sm={12}>
                <FormControl fullWidth={fullWidth} error={fik?.error}>
                    <FormGroup row>
                        {getOptions() && (getOptions() as string[]).map(option => {
                            return <FormControlLabel key={option}
                                control={<Checkbox color="primary"
                                    value={fik?.value}
                                    name={fik?.name}
                                    onChange={fik?.onchange}
                                />}
                                label={option}
                            />
                        })}
                    </FormGroup>
                    {fik?.error && <FormHelperText>{fik?.helperText}</FormHelperText>}
                </FormControl >

            </Grid>
        </>}
    </Grid>
}

export default function AppTypeCheckbox() {
    const [newOption, setNewOption] = useState('');
    const { livePreview: { checkboxOptions } } = useAuthState();
    const dispatch = useAuthDispatch();

    const handleAddClick = () => {
        if (newOption !== '') {
            const options = checkboxOptions ? checkboxOptions.concat(newOption) : [newOption];
            dispatch({ type: "PREVIEW_CHECKBOX_OPTIONS", payload: { checkboxOptions: options } })
            setNewOption('');
        }
    }

    const handleChange = (e: any) => {
        setNewOption(e.target.value);
    }

    const handleRemove = (option: string) => {
        const options = checkboxOptions.filter((d: string) => d !== option);
        dispatch({ type: "PREVIEW_CHECKBOX_OPTIONS", payload: { checkboxOptions: options } })
    }

    return <Grid container spacing={3}>
        <Grid item sm={12}>
            <InputLabel style={{ color: colors.primary }}>Properties Section</InputLabel>
        </Grid>
        <Grid item sm={12}>
            <AppInput placeholder="New option" onChange={handleChange} value={newOption} />
            <IconButton color="primary" aria-label="upload picture" component="span" onClick={handleAddClick}>
                <AddCircleIcon style={{ color: colors.primary }} />
            </IconButton>
        </Grid>
        <Grid item xs={12} sm={6} style={{
            overflow: "auto",
            height: 300,
        }}>
            {checkboxOptions && (checkboxOptions as string[]).map(option => option && <AppOptionList text={option} handleRemove={() => handleRemove(option)} />)}
        </Grid>
    </Grid>
}