import { Grid, InputLabel, Tooltip } from "@material-ui/core";
import HelpOutlinedIcon from "@material-ui/icons/HelpOutlined";
import React, { useState } from "react";

import colors from "../../../config/colors";
import { useAuthDispatch, useAuthState } from "../../../context";
import AppInput from "../../landing/AppInput";
import AppRadioButton from "../../landing/AppRadioButton";
import AppStarHoverRating from "./AppStarHoverRating";
import { FormikType } from "../../../utils/formik/FormikValidationFields";
import AppToggleButtonGroup from "../../landing/AppToggleButtonGroup";

export function LivePreview({
  fik,
  questionTitle,
  questionHint,
  isRequired,
  optionsFromApi,
}: {
  fik?: FormikType;
  questionTitle: string;
  questionHint: string;
  isRequired: boolean;
  optionsFromApi?: { min: number; max: number; step: number; count: number };
}) {
  const {
    livePreview: {
      ratingType,
      numberMin,
      numberMax,
      numberStep,
      starMax,
      starStep,
    },
  } = useAuthState();

  const [numberValue, setNumberValue] = useState("");

  const handleNumberChange = (e: React.MouseEvent<HTMLElement>, newValue: string) => {
      setNumberValue(newValue);
  };

  const [starValue, setValue] = React.useState<number | null>(0);

  const handleStarChange = (e:React.ChangeEvent<any>, newValue: number | null) => {
    setValue(newValue);
  }

  const getElement = () => {
    if (optionsFromApi) {
      if (optionsFromApi.min) {
        return (
          <AppToggleButtonGroup
            min={optionsFromApi.min}
            max={optionsFromApi.max}
            step={optionsFromApi.step}
            name={fik?.name}
            value={fik?.value}
            handleChange={fik?.onchange}
            error={fik?.error}
            helperText={fik?.helperText}
          />
        );
      } else {
        return <AppStarHoverRating max={optionsFromApi.count} step={1} value={starValue} handleChange={handleStarChange} />;
      }
    } else {
      if (ratingType === "Number") {
        return (
          <AppToggleButtonGroup
            min={numberMin}
            max={numberMax}
            step={numberStep}
            value={numberValue}
            handleChange={handleNumberChange}
          />
        );
      } else {
        return <AppStarHoverRating max={starMax} step={starStep} value={starValue} handleChange={handleStarChange} />;
      }
    }
  };

  return (
    <Grid container spacing={1}>
      {questionTitle && (
        <>
          <Grid item sm={12}>
            <Grid
              container
              spacing={2}
              direction="row"
              justifyContent="flex-start"
              alignItems="center"
            >
              <Grid item>
                <InputLabel>{questionTitle}{isRequired && <span style={{color: "red", fontSize: 22}}>  *</span>}</InputLabel>
              </Grid>
              {questionHint && (
                <Grid item>
                  <Tooltip title={questionHint} placement="right">
                    <HelpOutlinedIcon
                      style={{ color: colors.tooltip }}
                      fontSize="small"
                    />
                  </Tooltip>
                </Grid>
              )}
            </Grid>
          </Grid>
          <Grid item sm={12}>
            {getElement()}
          </Grid>
        </>
      )}
    </Grid>
  );
}

export default function AppTypeRating() {
  const {
    livePreview: {
      ratingType,
      numberMin,
      numberMax,
      numberStep,
      starMax,
      starStep,
    },
  } = useAuthState();
  const dispatch = useAuthDispatch();
  const [type, setType] = useState(ratingType);
  const [min, setMin] = useState(numberMin);
  const [max, setMax] = useState(type === "Star" ? starMax : numberMax);
  const [step, setStep] = useState(type === "Star" ? starStep : numberStep);

  const handleTypeChange = (e: any) => {
    setType(e.target.value);
    dispatch({
      type: "PREVIEW_RATING_TYPE_PROPERTIES",
      payload: { ratingType: e.target.value },
    });
    console.log(e.target.value);
    if (e.target.value === "Star") {
      dispatch({
        type: "PREVIEW_STAR_PROPERTIES",
        payload: { starMax, starStep },
      });
    } else {
      dispatch({
        type: "PREVIEW_NUMBER_PROPERTIES",
        payload: { numberMin, numberMax, numberStep },
      });
    }
  };

  const handleChange = (e: React.ChangeEvent<any>) => {
    let targetvalue = Number(e.target.value);
    if (e.target.value === "" || e.target.value === undefined) targetvalue = 0;
    let numberMin = Number(min);
    let numberMax = Number(max);
    let numberStep = Number(step);
    let starMax = Number(max);
    let starStep = Number(step);
    switch (e.target.name) {
      case "min":
        numberMin = targetvalue <= 0 ? 1 : targetvalue;
        setMin(numberMin);
        if (numberMin >= numberMax) {
          numberMax = numberMin + 1;
          starMax = numberMax;
          setMax(numberMax);
        }
        break;
      case "max":
        numberMax = targetvalue < 2 ? 2 : targetvalue;
        starMax = numberMax;
        setMax(numberMax);
        if (numberMax <= numberMin) {
          numberMin = numberMax - 1;
          setMin(numberMin);
        }
        break;
      case "step":
        numberStep = 1;
        starStep = targetvalue <= 0 ? 0.5 : targetvalue > 1 ? 1 : targetvalue;
        setStep(type === "Star" ? starStep : numberStep);
        break;
    }
    if (type === "Star") {
      dispatch({
        type: "PREVIEW_STAR_PROPERTIES",
        payload: { starMax, starStep },
      });
    } else {
      dispatch({
        type: "PREVIEW_NUMBER_PROPERTIES",
        payload: { numberMin, numberMax, numberStep },
      });
    }
  };

  return (
    <Grid container spacing={3}>
      <Grid item sm={12}>
        <InputLabel style={{ color: colors.primary }}>
          Properties Section
        </InputLabel>
      </Grid>
      <Grid item xs={12} sm={8}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={4}>
            <AppInput
              label="Min"
              type="number"
              placeholder="Min"
              name="min"
              onChange={handleChange}
              value={min}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <AppInput
              label="Max"
              type="number"
              placeholder="Max"
              name="max"
              onChange={handleChange}
              value={max}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <AppInput
              label="Step"
              type="number"
              placeholder="Step"
              name="step"
              onChange={handleChange}
              value={step}
            />
          </Grid>
          <Grid item sm={12}>
            <AppRadioButton
              handleChange={handleTypeChange}
              value={type}
              options={["Star", "Number"]}
            />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
