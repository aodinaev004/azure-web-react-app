import { Grid, InputLabel } from "@material-ui/core";
import { useState } from "react";
import Tooltip from "@material-ui/core/Tooltip";
import HelpOutlinedIcon from "@material-ui/icons/HelpOutlined";

import AppInput from "../../landing/AppInput";
import { useAuthDispatch, useAuthState } from "../../../context";
import AppSlider from "./AppSlider";
import colors from "../../../config/colors";
import { FormikType } from "../../../utils/formik/FormikValidationFields";

export function LivePreview({
  fik,
  questionTitle,
  questionHint,
  isRequired,
  value,
  handleChange,
  optionsFromApi,
}: {
  fik?: FormikType;
  questionTitle: string;
  questionHint: string;
  isRequired: boolean;
  value?: number | number[];
  handleChange?: (event: any, newValue: number | number[]) => void;
  optionsFromApi?: {
    defaultValue: number;
    min: number;
    max: number;
    step: number;
  };
}) {
  const {
    livePreview: { sliderDefaultValue, sliderMin, sliderMax, sliderStep },
  } = useAuthState();
//   const [value, setValue] = useState<number | Array<number>>(0);

//   const handleChange = (event: any, newValue: number | number[]) => {
//     setValue(newValue);

//     // fik?.onchange(newValue);
//   };

  return (
    <Grid container spacing={1}>
      {questionTitle && (
        <>
          <Grid item sm={12}>
            <Grid
              container
              spacing={2}
              direction="row"
              justifyContent="flex-start"
              alignItems="center"
            >
              <Grid item>
                <InputLabel>
                  {questionTitle}
                  {isRequired && (
                    <span style={{ color: "red", fontSize: 22 }}> *</span>
                  )}
                </InputLabel>
              </Grid>
              {questionHint && (
                <Grid item>
                  <Tooltip title={questionHint} placement="right">
                    <HelpOutlinedIcon
                      style={{ color: colors.tooltip }}
                      fontSize="small"
                    />
                  </Tooltip>
                </Grid>
              )}
            </Grid>
          </Grid>
          <Grid item sm={12}>
            <AppSlider
              defaultValue={
                optionsFromApi
                  ? optionsFromApi.defaultValue
                  : sliderDefaultValue
              }
              min={optionsFromApi ? optionsFromApi.min : sliderMin}
              max={optionsFromApi ? optionsFromApi.max : sliderMax}
              step={optionsFromApi ? optionsFromApi.step : sliderStep}
              name={fik?.name}
              value={value}
              handleSliderChange={handleChange}
              error={fik?.error}
              helperText={fik?.helperText}
            />
          </Grid>
        </>
      )}
    </Grid>
  );
}

export default function AppTypeSlider() {
  const {
    livePreview: { sliderDefaultValue, sliderMin, sliderMax, sliderStep },
  } = useAuthState();
  const dispatch = useAuthDispatch();
  const [defaultValue, setDefaultValue] = useState(sliderDefaultValue);
  const [min, setMin] = useState(sliderMin);
  const [max, setMax] = useState(sliderMax);
  const [step, setStep] = useState(sliderStep);

  const handleChange = (e: any) => {
    let targetvalue = Number(e.target.value);
    if (e.target.value === "" || e.target.value === undefined) targetvalue = 0;
    let sliderDefaultValue = Number(defaultValue);
    let sliderMin = Number(min);
    let sliderMax = Number(max);
    let sliderStep = Number(step);
    switch (e.target.name) {
      case "default":
        sliderDefaultValue = targetvalue;
        if (sliderDefaultValue < sliderMin) {
          sliderMin = sliderDefaultValue;
          setMin(sliderMin);
        }
        if (sliderDefaultValue > sliderMax) {
          sliderMax = sliderDefaultValue;
          setMax(sliderMax);
        }
        setDefaultValue(sliderDefaultValue);
        break;
      case "min":
        sliderMin = targetvalue;
        setMin(sliderMin);
        if (sliderMin > sliderDefaultValue) {
          sliderDefaultValue = sliderMin;
          setDefaultValue(sliderMin);
        }
        if (sliderMin >= sliderMax) {
          sliderMax = sliderMin + 1;
          setMax(sliderMax);
        }
        break;
      case "max":
        sliderMax = targetvalue;
        setMax(sliderMax);
        if (sliderMax < sliderDefaultValue) {
          sliderDefaultValue = sliderMax;
          setDefaultValue(sliderMax);
        }
        if (sliderMax <= sliderMin) {
          sliderMin = sliderMax - 1;
          setMin(sliderMin);
        }
        break;
      case "step":
        sliderStep = targetvalue === 0 ? 1 : targetvalue;
        setStep(sliderStep);
        break;
    }
    dispatch({
      type: "PREVIEW_SLIDER_PROPERTIES",
      payload: { sliderDefaultValue, sliderMin, sliderMax, sliderStep },
    });
  };

  return (
    <Grid container spacing={3}>
      <Grid item sm={12}>
        <InputLabel style={{ color: `${colors.primary}` }}>
          Properties Section
        </InputLabel>
      </Grid>
      <Grid item xs={12} sm={8}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={3}>
            <AppInput
              label="Default value"
              type="number"
              placeholder="Default value"
              name="default"
              onChange={handleChange}
              value={defaultValue}
            />
          </Grid>
          <Grid item xs={12} sm={3}>
            <AppInput
              label="Min"
              type="number"
              placeholder="Min"
              name="min"
              onChange={handleChange}
              value={min}
            />
          </Grid>
          <Grid item xs={12} sm={3}>
            <AppInput
              label="Max"
              type="number"
              placeholder="Max"
              name="max"
              onChange={handleChange}
              value={max}
            />
          </Grid>
          <Grid item xs={12} sm={3}>
            <AppInput
              label="Step"
              type="number"
              placeholder="Step"
              name="step"
              onChange={handleChange}
              value={step}
            />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
