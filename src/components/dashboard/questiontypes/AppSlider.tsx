import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import { FormControl, FormHelperText, Grid } from '@material-ui/core';

function valuetext(value: number) {
    return `${value}°C`;
}

interface SliderProperties {
    valueLabelDisplay?: "auto" | "on" | "off" | undefined;
    defaultValue?: number;
    step?: number;
    min?: number;
    max?: number;
    value?: number | number[];
    handleSliderChange?: (event: any, newValue: number | number[]) => void;
    name?: string;
    error?: boolean;
    helperText?: string;
    fullWidth?: boolean;
}

export default function AppSlider({ valueLabelDisplay, defaultValue = 0, step, min, max, handleSliderChange, value = 0, name, error, helperText, fullWidth }: SliderProperties) {
console.log("value", value);
    return <FormControl fullWidth error={error}>
        <Grid container
            direction="row"
            justifyContent="flex-start"
            alignItems="center"
            spacing={3}>
            <Grid item sm={11}>
                <Slider
                    defaultValue={defaultValue}
                    getAriaValueText={valuetext}
                    aria-labelledby="discrete-slider"
                    valueLabelDisplay={valueLabelDisplay}
                    step={step}
                    marks
                    min={min}
                    max={max}
                    onChange={handleSliderChange}
                    name={name}
                />
            </Grid>
            <Grid item sm={1}>
                <Typography>{value}</Typography>
            </Grid>
        </Grid>
        {error && <FormHelperText>{helperText}</FormHelperText>}
    </FormControl>
}