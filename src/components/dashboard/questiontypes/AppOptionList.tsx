import { createStyles, makeStyles, Theme } from "@material-ui/core";
import { Checkbox, Grid, InputLabel } from "@material-ui/core";
import colors from "../../../config/colors";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        label: {
            display: 'flex',
            alignItems: 'center'
        }

    })
)

export default function AppOptionList({ text, handleRemove }: { text: string, handleRemove: () => void }) {
    const classes = useStyles();

    return <>
        <Grid container spacing={0}>
            <Grid item sm={9} className={classes.label}>
                <InputLabel>{text}</InputLabel>
            </Grid>
            <Grid item sm={3}>
                <Checkbox
                    defaultChecked
                    indeterminate
                    inputProps={{ 'aria-label': 'indeterminate checkbox' }}
                    onClick={handleRemove}
                    style={{ color: colors.secondary }}
                />
            </Grid>
        </Grid>
    </>
}