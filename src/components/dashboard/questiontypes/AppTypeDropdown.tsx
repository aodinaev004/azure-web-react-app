import { Grid, IconButton, InputLabel } from "@material-ui/core";
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { useState } from "react";
import Tooltip from '@material-ui/core/Tooltip';
import HelpOutlinedIcon from '@material-ui/icons/HelpOutlined';

import AppInput from "../../landing/AppInput";
import AppOptionList from "./AppOptionList";
import { useAuthDispatch, useAuthState } from '../../../context';
import AppSelect from "../AppSelect";
import colors from "../../../config/colors";
import { FormikType } from "../../../utils/formik/FormikValidationFields";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        appSelect: {
            minWidth: 200
        }
    })
)

interface LivePreviewType {
    fik?: FormikType;
    questionTitle: string;
    questionHint: string;
    isRequired: boolean;
    optionsFromApi?: string[];
    handleSelectChange?: (e: React.ChangeEvent<any>) => void;
    selectedValue?: string;
}

export function LivePreview({ fik, questionTitle, questionHint, isRequired, optionsFromApi, handleSelectChange, selectedValue }: LivePreviewType) {
    const classes = useStyles();
    const { livePreview: { dropdownOptions } } = useAuthState();

    const getElement = () => {
        let newOptions: { value: string, text: string, icon?: React.ReactNode }[] = [];
        let val = '';

        if (optionsFromApi) {
            optionsFromApi.map((option: string) => newOptions.push({ value: option, text: option }))
        } else {
            dropdownOptions.map((option: string) => newOptions.push({ value: option, text: option }))
        }
        if (fik) {
            return <AppSelect options={newOptions}
                name={fik?.name}
                value={fik?.value}
                handleChange={fik?.onchange}
                error={fik?.error}
                defaultValue={val}
                helperText={fik?.helperText}
                className={classes.appSelect}
            />
        } else {
            return <AppSelect options={newOptions}
                value={selectedValue}
                handleChange={handleSelectChange}
                defaultValue={val}
                className={classes.appSelect}
            />
        }
    }

    return <Grid container spacing={1}>
        {questionTitle && <>
            <Grid item sm={12}>
                <Grid container spacing={2}
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="center">
                    <Grid item>
                        <InputLabel>{questionTitle}{isRequired && <span style={{color: "red", fontSize: 22}}>  *</span>}</InputLabel>
                    </Grid>
                    {questionHint && <Grid item>
                            <Tooltip title={questionHint} placement="right">
                                <HelpOutlinedIcon style={{ color: colors.tooltip }} fontSize="small" />
                            </Tooltip>
                    </Grid>}
                </Grid>
            </Grid>
            <Grid item sm={12}>
                {getElement()}
            </Grid>
        </>}
    </Grid>
}

export default function AppTypeDropdown() {
    const [newOption, setNewOption] = useState('');
    const { livePreview: { dropdownOptions } } = useAuthState();
    const dispatch = useAuthDispatch();

    const handleAddClick = () => {
        if (newOption !== '') {
            const options = dropdownOptions ? dropdownOptions.concat(newOption) : [newOption];
            dispatch({ type: "PREVIEW_DROPDOWN_OPTIONS", payload: { dropdownOptions: options } })
            setNewOption('');
        }
    }

    const handleChange = (e: any) => {
        setNewOption(e.target.value);
    }

    const handleRemove = (option: string) => {
        const options = dropdownOptions.filter((d: string) => d !== option);
        dispatch({ type: "PREVIEW_DROPDOWN_OPTIONS", payload: { dropdownOptions: options } })
    }

    return <Grid container spacing={3}>
        <Grid item sm={12}>
            <InputLabel style={{ color: colors.primary }}>Properties Section</InputLabel>
        </Grid>
        <Grid item sm={12}>
            <AppInput placeholder="New option" onChange={handleChange} value={newOption} />
            <IconButton color="primary" aria-label="upload picture" component="span" onClick={handleAddClick}>
                <AddCircleIcon style={{ color: colors.primary }} />
            </IconButton>
        </Grid>
        <Grid item xs={12} sm={6} style={{
            overflow: "auto",
            height:300,
        }}>
            {dropdownOptions && (dropdownOptions as string[]).map(option => option && <AppOptionList text={option} handleRemove={() => handleRemove(option)} />)}
        </Grid>
    </Grid>
}