import { Grid, InputLabel } from "@material-ui/core";
import Tooltip from '@material-ui/core/Tooltip';
import HelpOutlinedIcon from '@material-ui/icons/HelpOutlined';

import colors from "../../../config/colors";
import FormikDateTimePicker from "../../landing/AppDateTimePicker";

export function LivePreview({ validation, questionTitle, questionHint, isRequired }
    : { validation?: boolean, questionTitle: string, questionHint: string, isRequired: boolean }) {

    return <Grid container spacing={1}>
        {questionTitle && <>
            <Grid item sm={12}>
                <Grid container spacing={2}
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="center">
                    <Grid item>
                        <InputLabel>{questionTitle}{isRequired && <span style={{color: "red", fontSize: 22}}>  *</span>}</InputLabel>
                    </Grid>
                    {questionHint && <Grid item>
                        <Tooltip title={questionHint} placement="right">
                            <HelpOutlinedIcon style={{ color: colors.tooltip }} fontSize="small" />
                        </Tooltip>
                    </Grid>}
                </Grid>
            </Grid>
            <Grid item sm={12}>
                <FormikDateTimePicker />
            </Grid>
        </>}
    </Grid>
}

export default function AppTypeDateTime() {
    return <></>
}