import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import VisibilityIcon from '@material-ui/icons/Visibility';
import IconButton from '@material-ui/core/IconButton';
import AddCircleOutlinedIcon from '@material-ui/icons/AddCircleOutlined';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import EditIcon from '@material-ui/icons/Edit';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import DeleteIcon from '@material-ui/icons/Delete';
import { useHistory } from 'react-router-dom';

import { useAuthDispatch, useAuthState, getSurvey } from '../../context';
import colors from '../../config/colors';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: '28px',
      bottom: -8,
      right: 5,
      width: 220,
      display: "flex",
      marginRight: 8,
      marginLeft: "auto",
      marginTop: -15,
      marginBottom: 6,
      // backgroundColor: 'white',
      // position: 'absolute',
    },
    iconBtn: {
      width: 35,
      height: 35,
      // backgroundColor: 'white'
    }
  })
);

const AppEmployeeListAction = ({ surveyID }: { surveyID: number }) => {
  const classes = useStyles();
  const dispatch = useAuthDispatch();
  const { login: { token } } = useAuthState();
  const history = useHistory();

  const handleView = () => {
    dispatch({ type: "CREATED_SURVEY_ID", id: surveyID })
    getSurvey(dispatch, { id: surveyID, token })
    history.push('/dashboard/create-custom-survey')
  }

  return (
    <Grid
      container
      justifyContent="center"
      alignItems="center"
      className={classes.root}
    >
      <IconButton aria-label="view" className={classes.iconBtn} onClick={handleView} >
        <VisibilityIcon fontSize="medium" htmlColor={colors.blue} />
      </IconButton>
      <IconButton aria-label="add" className={classes.iconBtn}  >
        <AddCircleOutlinedIcon fontSize="medium" htmlColor={colors.success} />
      </IconButton>
      <IconButton aria-label="upload" className={classes.iconBtn} >
        <CloudUploadIcon fontSize="medium" htmlColor={colors.black} />
      </IconButton>
      <IconButton aria-label="edit" className={classes.iconBtn} >
        <EditIcon fontSize="medium" />
      </IconButton>
      <IconButton aria-label="copy" className={classes.iconBtn}  >
        <InsertDriveFileIcon fontSize="medium" />
      </IconButton>
      <IconButton aria-label="delete" className={classes.iconBtn}  >
        <DeleteIcon fontSize="medium" htmlColor={colors.error} />
      </IconButton>
    </Grid>
  );
}

export default AppEmployeeListAction;