import { Grid } from '@material-ui/core';
import Typography from "@material-ui/core/Typography";
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import AppSurveyTotalCount from './AppSurveyTotalCount';
import colors from '../../config/colors';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        grigTotal:{
            borderRadius: 5,
            position: 'relative',
            border: '2px solid '+colors.primary,
            height: "80px",
            marginTop: 15,
            padding: 10
        },
        gridTotalLabel:{
            backgroundColor: '#fcfcfc',
            color: 'rgb(109, 171, 228)',
            position: 'absolute',
            top: '-11px',
            width: '110px'
        }
    }),
);



const AppSurveyTotal = () => {
  const classes = useStyles();

  return (
    <>
      <Grid 
        container
        alignItems="center"
        className={classes.grigTotal} 
      > 
          <Typography
              component="p"
              variant="subtitle2"
            className={classes.gridTotalLabel}
          >
            Total Number of
          </Typography>
          <AppSurveyTotalCount title='Respondents' total={10}/>
          <AppSurveyTotalCount title='Responses' total={20}/>
          <AppSurveyTotalCount title='Views' total={18}/>
      </Grid>
    </>
  );
}

export default AppSurveyTotal;