import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import colors from '../../config/colors';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        chips: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        chip: {
            margin: 2,
        },
        noLabel: {
            marginTop: theme.spacing(3),
        },
        '@global': {
            '*::-webkit-scrollbar': {
                width: 6,
                height: 6
            },
            '*::-webkit-scrollbar-thumb': {
                borderRadius: 10,
                backgroundColor: colors.scroll
            }
        },
    }),
);

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

interface SelectInterface {
    options: string[];
    placeholder?: string;
    label?: string;
    className?: string;
    fullWidth?: boolean;
    handleChange?: (event: React.ChangeEvent<{
        value: unknown;
    }>) => void;
    selectedOption: string[];
    name?: string;
    error?: boolean;
    helperText?: string;
}

export default function AppMultipleSelect({ name, error, helperText, options, placeholder, label, className, fullWidth, selectedOption, handleChange }: SelectInterface) {
    const classes = useStyles();
    return (
        <div>
            <FormControl fullWidth={fullWidth}>
                {label && <InputLabel id={`mutiple-chip-${label}`} style={{ color: colors.primary }}>{label}</InputLabel>}
                <Select
                    labelId={`mutiple-chip-${label}`}
                    id="mutiple-chip"
                    multiple
                    placeholder={placeholder}
                    value={selectedOption}
                    onChange={handleChange}
                    input={<Input id="select-multiple-chip" />}
                    renderValue={(selected) => (
                        <div className={classes.chips}>
                            {(selected as string[]).map((value) => (
                                <Chip key={value} label={value} className={classes.chip} />
                            ))}
                        </div>
                    )}
                    MenuProps={MenuProps}
                    className={className}
                >
                    {options.map((option) => (
                        <MenuItem key={option} value={option}>
                            <Checkbox checked={selectedOption.indexOf(option) > -1} />
                            <ListItemText primary={option} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </div>
    );
}
