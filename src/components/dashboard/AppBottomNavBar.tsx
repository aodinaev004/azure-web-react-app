import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import MenuIcon from "@material-ui/icons/Menu";

const useStyles = makeStyles({
    root: {
        width: "100%",
    },
});

export default function AppBottomNavBar({ value, handleBottomMenuOpen }: {
    value?: string,
    handleBottomMenuOpen?: (event: React.ChangeEvent<{}>, newValue: string) => void
}) {
    const classes = useStyles();
    // const [value, setValue] = React.useState('menu');

    // const handleChange = (event: React.ChangeEvent<{}>, newValue: string) => {
    //     setValue(newValue);
    // };

    return (
        <BottomNavigation value={value} onChange={handleBottomMenuOpen} className={classes.root}>
            <BottomNavigationAction value="menu" icon={<MenuIcon color="primary" />} />
        </BottomNavigation>
    );
}
