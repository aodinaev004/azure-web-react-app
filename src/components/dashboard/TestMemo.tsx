import React from "react";
import { Grid, InputLabel, Tooltip } from "@material-ui/core";
import HelpOutlinedIcon from "@material-ui/icons/HelpOutlined";

import colors from "../../config/colors";
import AppInput from "../landing/AppInput";

const QuestionData = ({
  questionTitle,
  isRequired,
  questionHint,
  component,
}: {
  questionTitle: string;
  isRequired: boolean;
  questionHint: string;
  component: any;
}) => {
  return (
    <Grid item sm={12} style={{ marginTop: 20 }}>
      <Grid container spacing={1}>
        {questionTitle && (
          <>
            <Grid item sm={12}>
              <Grid
                container
                spacing={2}
                direction="row"
                justifyContent="flex-start"
                alignItems="center"
              >
                <Grid item>
                  <InputLabel>
                    {questionTitle}
                    {isRequired && (
                      <span style={{ color: "red", fontSize: 22 }}> *</span>
                    )}
                  </InputLabel>
                </Grid>
                {questionHint && (
                  <Grid item>
                    <Tooltip title={questionHint} placement="right">
                      <HelpOutlinedIcon
                        style={{ color: colors.tooltip }}
                        fontSize="small"
                      />
                    </Tooltip>
                  </Grid>
                )}
              </Grid>
            </Grid>
            <Grid item sm={12}>
              {component}
            </Grid>
          </>
        )}
      </Grid>
    </Grid>
  );
};

export const InputMemo = React.memo(
  ({
    name,
    value,
    handleChange,
    error,
    helperText,
  }: {
    name: string;
    value: string;
    handleChange?: (e: React.ChangeEvent<any>) => void;
    error?: boolean;
    helperText: string;
  }) => {
    return (
      <AppInput
        name={name}
        value={value}
        onChange={handleChange}
        error={error}
        helperText={helperText}
        fullWidth={true}
      />
    );
  }
);

export default QuestionData;
