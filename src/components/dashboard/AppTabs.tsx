import React, { useState } from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PhoneIcon from '@material-ui/icons/Phone';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import HelpIcon from '@material-ui/icons/Help';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import * as yup from 'yup'
import { OptionalObjectSchema, TypeOfShape } from "yup/lib/object";

import AppSurveyForm from './AppSurveyForm';
import AppAddQuestionForm from './AppAddQuestionForm';
import colors from '../../config/colors';
import AppQuestionPreview from './AppQuestionPreviewForm';
import { NewObject, QuestionType, SurveyType } from '../../config/types';
import { useAuthState } from '../../context';
import validationSchame from "../../utils/formik/FormikValidationFields";
import AppPreviewValidationForm from './AppPreviewValidationForm';
import { boolean, number } from 'yup/lib/locale';

interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-force-tabpanel-${index}`}
            aria-labelledby={`scrollable-force-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography component="div">{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: any) {
    return {
        id: `scrollable-force-tab-${index}`,
        'aria-controls': `scrollable-force-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
        "& .MuiTabs-indicator": {
            backgroundColor: colors.primary,
        },
        "& .Mui-selected": {
            color: colors.primary,
        }
    },
}));

export default function AppTabs() {
    const classes = useStyles();
    const [value, setValue] = useState(0);
    const [questions, setQuestions] = useState([] as QuestionType[]);

    const [validationFields, setValidationFields] = useState({});
    const [formikSchame, setFormikSchame] = useState({} as OptionalObjectSchema<NewObject, Record<string, any>, TypeOfShape<NewObject>>);
    const [formikMemo, setFormikMemo] = useState({} as NewObject);
    const [previewQuestionsForm, setPreviewQuestionsForm] = useState<JSX.Element | undefined>(undefined);
    const { surveys: { surveyData } } = useAuthState();

    const getOptions = (questionOptions: { title: string }[]) => {
        let options:{[key:string]:boolean} = {};
        const questionOpt = questionOptions || [];
        questionOpt.map(opt => options[opt.title] = false)
        return options;
    }

    let yupSchame = yup.object({})
    const getFields = (questions:QuestionType[]) => {
        let validFields: NewObject = {};
        let schame: NewObject = {};
        let memos: NewObject = {};
        let errors: NewObject = {};
        let sliderValues: NewObject = {};
        let starValues: NewObject = {};
        let numberValues: NewObject = {};
        console.log(questions);
        questions.map((question, index) => {
            const fieldName = `${question.type.toLowerCase()}${index}`;
            
            memos[fieldName] = true;
            errors[fieldName] = false;
            
            if (question.type.toLowerCase() === "checkbox") {
                validFields[fieldName] = getOptions(JSON.parse(question.body).choices);
            } else if (question.type.toLowerCase() === "rating") {
                validFields[fieldName] = 0
                numberValues[fieldName] = ""
            } else if (question.type.toLowerCase() === "star_rating") {
                validFields[fieldName] = 0
                starValues[fieldName] = 0
            } else if (question.type.toLowerCase() === "slider") {
                validFields[fieldName] = 0
                sliderValues[fieldName] = 0
            } else {
                validFields[fieldName] = '';
            }
        })
        
        setPreviewQuestionsForm(<AppPreviewValidationForm questions={questions} formErrors={errors} formInitialValues={validFields} sliderValues={sliderValues} starValues={starValues} numberValues={numberValues} />)
        yupSchame = yup.object(schame);

        setValidationFields(validFields);
        setFormikSchame(yupSchame);
        setFormikMemo(memos)
    }

    let survey: SurveyType = surveyData;
    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
        if (newValue === 2) {
            if (surveyData !== undefined && surveyData !== null) {
                setQuestions(survey.template.questions as QuestionType[]);
                getFields(survey.template.questions as QuestionType[]);
            }
        }
    };

    return (
        <div className={classes.root}>
            <AppBar position="static" color="default">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    variant="scrollable"
                    scrollButtons="on"
                    indicatorColor="primary"
                    textColor="primary"
                    aria-label="scrollable force tabs example"
                >
                    <Tab label="SURVEY" icon={<PhoneIcon />} {...a11yProps(0)} />
                    <Tab label="ADD QUESTION" icon={<HelpIcon />} {...a11yProps(1)} />
                    <Tab label="PREVIEW" icon={<PersonPinIcon />} {...a11yProps(2)} disabled={(!survey?.template?.questions || survey?.template?.questions.length === 0) ? true : false} />
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
                <AppSurveyForm />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <AppAddQuestionForm />
            </TabPanel>
            <TabPanel value={value} index={2}>
                <AppQuestionPreview previewQuestionsForm={previewQuestionsForm} />
            </TabPanel>
        </div>
    );
}
