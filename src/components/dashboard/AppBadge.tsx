import React from 'react';
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';

import colors from '../../config/colors';

const useStyles = (state: any) => makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& > *': {
        marginLeft: theme.spacing(0),
        marginTop: theme.spacing(1) + 2,
      },
      "& .MuiBadge-anchorOriginTopLeftRectangle": {
        transform: "scale(1) translate(0, -50%)",
        borderRadius: "5px",
        color: "#fff",
        backgroundColor: state,
        textTransform: "uppercase",
      }
    }
  }),
);

const badgeColors: { [key: string]: string } = {
  error: colors.error,
  draft: colors.draft,
  proccessing: colors.proccessing,
  success: colors.success,
}

export default function AppBadge({ state, childElement, onClick }: { state: string, childElement: React.ReactNode, onClick?: () => void }) {
  const classes = useStyles(badgeColors[state.toLowerCase()]);
  const stateToUpper = state.toUpperCase();

  return (
    <div className={classes().root} onClick={onClick}>
      <Badge style={{ width: "100%" }} badgeContent={stateToUpper} anchorOrigin={{ vertical: "top", horizontal: "left" }}>
        {childElement}
      </Badge>
    </div>
  );
}
