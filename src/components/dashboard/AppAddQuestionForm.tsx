import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined';
import RadioButtonCheckedOutlinedIcon from '@material-ui/icons/RadioButtonCheckedOutlined';
import StarBorderOutlinedIcon from '@material-ui/icons/StarBorderOutlined';
import ChatBubbleOutlineOutlinedIcon from '@material-ui/icons/ChatBubbleOutlineOutlined';
import ArrowDropDownOutlinedIcon from '@material-ui/icons/ArrowDropDownOutlined';
import FormatColorTextOutlinedIcon from '@material-ui/icons/FormatColorTextOutlined';
import CalendarTodayOutlinedIcon from '@material-ui/icons/CalendarTodayOutlined';
import LinearScaleOutlinedIcon from '@material-ui/icons/LinearScaleOutlined';
import { FormControl, FormHelperText, Grid, Input } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import SaveIcon from '@material-ui/icons/Save';

import AppSelect from "./AppSelect";
import AppTypeCheckbox, { LivePreview as CheckLivePreview } from "./questiontypes/AppTypeCheckbox";
import AppTypeComment, { LivePreview as CommentLivePreview } from "./questiontypes/AppTypeComment";
import AppTypeDropdown, { LivePreview as DropdownLivePreview } from "./questiontypes/AppTypeDropdown";
import AppTypeTextBox, { LivePreview as TextBoxLivePreview } from "./questiontypes/AppTypeTextBox";
import AppTypeDateTime, { LivePreview as DateTimeLivePreview } from "./questiontypes/AppTypeDateTime";
import AppTypeSlider, { LivePreview as SliderLivePreview } from "./questiontypes/AppTypeSlider";
import AppTypeRadioButton, { LivePreview as RadioLivePreview } from "./questiontypes/AppTypeRadioButton";
import AppTypeRating, { LivePreview as RatingLivePreview } from "./questiontypes/AppTypeRating";
import { createSurvey, getSurvey, useAuthDispatch, useAuthState } from '../../context';
import AppInput from "../landing/AppInput";
import AppCheckbox from "../landing/AppCheckbox";
import colors from "../../config/colors";
import { QuestionType, SurveyType } from '../../config/types';
import AppFormButton from '../landing/AppFormButton';
import { useFormik } from 'formik';
import { getValidationFields } from '../../utils/formik/FormikValidationFields';
import AppSnackbarAlert from '../common/AppSnackbarAlert';

const typeQuestionOptions = [
    { value: 'checkbox', text: 'Checkbox', icon: <CheckBoxOutlinedIcon fontSize="small" /> },
    { value: 'rating', text: 'Rating', icon: <StarBorderOutlinedIcon fontSize="small" /> },
    { value: 'radiobutton', text: 'Radio Button', icon: <RadioButtonCheckedOutlinedIcon fontSize="small" /> },
    { value: 'comment_box', text: 'Comment Box', icon: <ChatBubbleOutlineOutlinedIcon fontSize="small" /> },
    { value: 'dropdown', text: 'Dropdown', icon: <ArrowDropDownOutlinedIcon fontSize="small" /> },
    { value: 'text', text: 'Textbox', icon: <FormatColorTextOutlinedIcon fontSize="small" /> },
    { value: 'datetime', text: 'Date/Time', icon: <CalendarTodayOutlinedIcon fontSize="small" /> },
    { value: 'slider', text: 'Slider', icon: <LinearScaleOutlinedIcon fontSize="small" /> },
]

type acceptType = {
    checkboxOptions: string[],
    radioOptions: string[],
    dropdownOptions: string[],
    sliderMin: number,
    sliderMax: number,
    sliderStep: number,
    numberMin: number,
    numberMax: number,
    numberStep: number,
    starMax: number,
    starStep: number,
    ratingType: string
}

const questionsObject = {
    checkbox: <AppTypeCheckbox />,
    radiobutton: <AppTypeRadioButton />,
    comment_box: <AppTypeComment />,
    dropdown: <AppTypeDropdown />,
    text: <AppTypeTextBox />,
    datetime: <AppTypeDateTime />,
    slider: <AppTypeSlider />,
    rating: <AppTypeRating />,
    checkboxBody: function (accept: acceptType) {
        const { checkboxOptions } = accept;
        return this.bodyOptions(checkboxOptions);
    },
    radiobuttonBody: function (accept: acceptType) {
        const { radioOptions } = accept;
        return this.bodyOptions(radioOptions);
    },
    comment_boxBody: function () {
        return null;
    },
    dropdownBody: function (accept: acceptType) {
        const { dropdownOptions } = accept;
        return this.bodyOptions(dropdownOptions);
    },
    textBody: function () {
        return null;
    },
    datetimeBody: function () {
        return null;
    },
    sliderBody: function (accept: acceptType) {
        const { sliderMin, sliderMax, sliderStep } = accept;
        return JSON.stringify({
            min: sliderMin,
            max: sliderMax,
            step: sliderStep
        })
    },
    ratingBody: function (accept: acceptType) {
        const { ratingType, numberMin, numberMax, numberStep, starMax, starStep } = accept;
        if (ratingType === "Star") {
            return JSON.stringify({
                min: 0,
                max: starMax,
                step: starStep,
                type: "star"
            })
        } else {
            return JSON.stringify({
                min: numberMin,
                max: numberMax,
                step: numberStep,
                type: "number"
            })
        }
    },
    bodyOptions: function (options: string[]) {
        let choices: { title: string }[] = [];
        let body: { choices: { title: string }[] };
        options && options.map((option) => choices.push({ title: option }))
        body = { choices }
        return JSON.stringify(body);
    },
}

let sent = false
let emptyTitle = false
export default function AppAddQuestionForm() {
    const dispatch = useAuthDispatch();

    const [questionTitle, setTitle] = useState('');
    const [questionHint, setHint] = useState('');
    const [isRequired, setRequired] = useState(false);
    const [dropdownValue, setDropdownValue] = useState('');

    const [selectedOption, setSelectedOption] = useState("checkbox");
    const [component, setComponent] = useState(<AppTypeCheckbox />);
    const [livePreview, setLivePreview] = useState(<CheckLivePreview questionTitle={questionTitle} questionHint={questionHint} isRequired={isRequired} />)
    const {
        livePreview: { checkboxOptions, radioOptions, dropdownOptions, sliderMin, sliderMax, sliderStep, numberMin, numberMax, numberStep, starMax, starStep, ratingType },
        createSurvey: { loading, status, updateStatus, surveyId },
        surveys: { surveyData },
        login: { token }
    } = useAuthState();
    const [sliderValue, setSliderValue] = useState<number | number[]>(0);

    const handleSliderChange = (event: any, newValue: number | number[]) => {
        console.log(sliderValue);
        setSliderValue(newValue);
    };

    type qType = ("checkbox" | "radiobutton")
    type qBody = ("checkboxBody" | "radiobuttonBody")
    const handleChange = (e: React.ChangeEvent<any>) => {
        const type: qType = e.target.value;
        setSelectedOption(e.target.value);
        setComponent(questionsObject[type]);
        handleAllSets(e.target.value, questionTitle, questionHint, isRequired);
    }

    const handleDropdownChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setDropdownValue(event.target.value as string);
        console.log(event.target.value);
    }

    const handleAllSets = (questionType: string, questionTitle: string, questionHint: string, isRequired: boolean) => {
        const questionsLivePreview = {
            checkbox: <CheckLivePreview questionTitle={questionTitle} questionHint={questionHint} isRequired={isRequired} />,
            radiobutton: <RadioLivePreview questionTitle={questionTitle} questionHint={questionHint} isRequired={isRequired} />,
            comment_box: <CommentLivePreview questionTitle={questionTitle} questionHint={questionHint} isRequired={isRequired} />,
            dropdown: <DropdownLivePreview questionTitle={questionTitle} questionHint={questionHint} isRequired={isRequired} handleSelectChange={handleDropdownChange} selectedValue={dropdownValue} />,
            text: <TextBoxLivePreview questionTitle={questionTitle} questionHint={questionHint} isRequired={isRequired} />,
            datetime: <DateTimeLivePreview questionTitle={questionTitle} questionHint={questionHint} isRequired={isRequired} />,
            slider: <SliderLivePreview questionTitle={questionTitle} questionHint={questionHint} isRequired={isRequired} value={sliderValue} handleChange={handleSliderChange} />,
            rating: <RatingLivePreview questionTitle={questionTitle} questionHint={questionHint} isRequired={isRequired} />
        }
        setLivePreview(questionsLivePreview[questionType as qType])
    }

    const handleTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        handleAllSets(selectedOption, e.target.value, questionHint, isRequired);
        setTitle(e.target.value);
        formik.setFieldValue("questionTitle", e.target.value);
    }

    const handleHintChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        handleAllSets(selectedOption, questionTitle, e.target.value, isRequired);
        setHint(e.target.value);
    }

    const handleCheckboxChange = () => {
        handleAllSets(selectedOption, questionTitle, questionHint, !isRequired);
        setRequired(!isRequired);
    }

    useEffect(() => {
        if (selectedOption === "checkbox" && checkboxOptions.length !== 0){
            formik.setFieldValue("options", "checkbox")
        } else if (selectedOption === "radiobutton" && radioOptions.length !== 0){
            formik.setFieldValue("options", "radiobutton")
        } else if (selectedOption === "dropdown" && dropdownOptions.length !== 0){
            formik.setFieldValue("options", "dropdown")
        } else if (selectedOption === "checkbox" && checkboxOptions.length === 0){
            formik.setFieldValue("options", "")
        } else if (selectedOption === "radiobutton" && radioOptions.length === 0){
            formik.setFieldValue("options", "")
        } else if (selectedOption === "dropdown" && dropdownOptions.length === 0){
            formik.setFieldValue("options", "")
        } else {
            console.log(1);
            formik.setFieldValue("options", "valid")
        }
    }, [selectedOption, radioOptions, checkboxOptions, dropdownOptions])

    let formik = useFormik({
        validateOnChange: false,
        initialValues: {
            questionTitle: "",
            options: ""
        },
        validationSchema: getValidationFields(["questionTitle", "options"]),
        onSubmit: (values) => {
            sent = true;
            const bodyData = {
                checkboxOptions,
                radioOptions,
                dropdownOptions,
                sliderMin,
                sliderMax,
                sliderStep,
                numberMin,
                numberMax,
                numberStep,
                starMax,
                starStep,
                ratingType
            }
    
            const typeBody: qBody = `${selectedOption}Body` as qBody;
    
            let survey: SurveyType = { ...surveyData };
            if (survey.template === null) survey.template = {
                title: '',
                questions: [],
                logoImage: ''
            };
    
            let questions: QuestionType[] = survey?.template?.questions || [];
    
            questions = questions.sort((a,b) => a.orderPosition - b.orderPosition);
           
            const orderPosition = questions.length === 0 ? 1 : questions[questions.length - 1].orderPosition + 1;
    
            const type = (selectedOption === "rating" && ratingType === "Star") ? "STAR_RATING" : selectedOption.toUpperCase()
            const newQuestion: QuestionType = {
                id: null,
                title: questionTitle,
                description: questionHint,
                required: isRequired,
                logic: null,
                orderPosition,
                type,
                body: questionsObject[typeBody](bodyData),
                uid: orderPosition.toString()
            }
    
            questions.push(newQuestion);
            survey.template.questions = questions;
    
            createSurvey(dispatch, survey, token);
        }
    })

    useEffect(() => {
        if (!loading) {
            formik.setFieldValue("options", "")
            formik.setFieldValue("questionTitle", "");
            emptyTitle = true
            setTitle('');
            setHint('');
            setRequired(false);
            handleAllSets(selectedOption, '', '', false)
            dispatch({ type: "PREVIEW_CHECKBOX_OPTIONS", payload: { checkboxOptions: [] } });
            dispatch({ type: "PREVIEW_RADIO_OPTIONS", payload: { radioOptions: [] } });
            dispatch({ type: "PREVIEW_DROPDOWN_OPTIONS", payload: { dropdownOptions: [] } });
            dispatch({ type: "PREVIEW_SLIDER_PROPERTIES", payload: { sliderDefaultValue: 0, sliderMin: 1, sliderMax: 10, sliderStep: 1 } });
            dispatch({ type: "PREVIEW_STAR_PROPERTIES", payload: { starMax: 5, starStep: 1 } });
            dispatch({ type: "PREVIEW_NUMBER_PROPERTIES", payload: { numberMin: 1, numberMax: 5, numberStep: 1 } });
        }
    }, [loading]);

    // useEffect(() => {
    //     // formik.setFieldValue("questionTitle", "x");
    //     // formik.setFieldTouched("questionTitle");
    //     formik.setFieldTouched("questionTitle");
    //     // formik.setFieldValue("questionTitle", "");
    // },[])

    return <form onSubmit={formik.handleSubmit}>
        {
            updateStatus && <AppSnackbarAlert type="success" text="snackbar_alert_success" />
        }
        <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
                <Grid container spacing={3}>
                    {/* <Grid item sm={4}>
                        <AppSelect asterisk label="Question Type" options={typeQuestionOptions} value={selectedOption} handleChange={handleChange} fullWidth={true} />
                    </Grid>
                    <Grid item sm={8}>
                        <AppInput asterisk label="Title" fullWidth={true} onChange={handleTitleChange} value={questionTitle} />
                    </Grid>
                    <Grid item sm={8}>
                        <AppInput label="Hint Text" onChange={handleHintChange} value={questionHint} fullWidth={true} />
                    </Grid>
                    <Grid item sm={4}>
                        <AppCheckbox label='Is Required' checked={isRequired} handleChange={handleCheckboxChange} />
                    </Grid>
                    <Grid item xs={12} style={{ display: 'flex', justifyContent: 'flex-end' }}>
                        <Grid item xs={6}>
                            {component}
                        </Grid>
                        <Grid item sm={6} style={{ display: 'flex', alignItems: 'center' }}>
                            <AppFormButton text="Save" startIcon={<SaveIcon />} onClick={handleSaveBtnClick} loading={loading} />
                        </Grid>
                    </Grid> */}
                    <Grid item sm={4}>
                        <AppSelect asterisk label="Question Type" options={typeQuestionOptions} value={selectedOption} handleChange={handleChange} fullWidth={true} />
                    </Grid>
                    <Grid item sm={8}>
                        <AppInput asterisk 
                            label="Title" 
                            fullWidth={true} 
                            name="questionTitle" 
                            onChange={handleTitleChange} 
                            value={questionTitle} 
                            error={formik.touched.questionTitle && Boolean(formik.errors.questionTitle)}
                            helperText={`${formik.touched.questionTitle && formik.errors.questionTitle}`}
                        />
                    </Grid>
                    <Grid item sm={8}>
                        <AppInput label="Hint Text" onChange={handleHintChange} value={questionHint} fullWidth={true} />
                    </Grid>
                    <Grid item sm={4}>
                        <AppCheckbox label='Is Required' checked={isRequired} handleChange={handleCheckboxChange} />
                    </Grid>
                    <Grid item xs={12} style={{ display: 'flex', justifyContent: 'flex-end' }}>
                        <Grid item xs={6} style={{display: "relative"}}>
                            <FormControl error={formik.touched.options && Boolean(formik.errors.options)} style={{top: 100}}>
                                {formik.touched.options && Boolean(formik.errors.options) && <FormHelperText>{formik.touched.options && formik.errors.options}</FormHelperText>}
                            </FormControl >
                            {component}
                        </Grid>
                        <Grid item sm={6} style={{ display: 'flex', alignItems: 'center' }}>
                            <AppFormButton text="Save" startIcon={<SaveIcon />} loading={loading} />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12} sm={6}>
                <FormControl style={{ marginBottom: 20, color: `${colors.primary}` }}>Live Preview</FormControl>
                {livePreview}
            </Grid>
        </Grid>
    </form>
}