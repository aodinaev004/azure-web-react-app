import { makeStyles, Theme } from "@material-ui/core";
import { Grid } from "@material-ui/core";
import { useEffect, useState } from "react";
import { useFormik } from "formik";
import SaveIcon from '@material-ui/icons/Save';

import { createSurvey, getSurvey, useAuthDispatch, useAuthState } from '../../context';
import { getValidationFields } from "../../utils/formik/FormikValidationFields";
import AppInput from "../landing/AppInput";
import AppMultipleSelect from "./AppMultipleSelect";
import AppSelect from "./AppSelect";
import AppUploadFiles from "./AppUploadFiles";
import AppFormButton from "../landing/AppFormButton";
import AppSuspenseLoading from "../common/AppSuspenseLoading";
import AppSnackbarAlert from "../common/AppSnackbarAlert";
import { blobToBase64, base64ToBlob } from "../../utils/converter/index";
import { SurveyType } from "../../config/types";

const useStyles = makeStyles((theme: Theme) => ({
    uploadImages: {
        width: "300px",
    },
    bottomSpacing: {
        marginBottom: theme.spacing(2)
    }
}))

const languageOptions = [{
    value: "EN",
    text: "English"
}, {
    value: "FR",
    text: "Franch"
}]

const tagsOptions = [
    'Oliver Hansen',
    'Van Henry',
    'April Tucker',
    'Ralph Hubbard',
    'Omar Alexander',
    'Carlos Abbott',
    'Miriam Wagner',
    'Bradley Wilkerson',
    'Virginia Andrews',
    'Kelly Snyder',
];

const transactionModeOptions = [
    { value: '1', text: 'One' },
    { value: '2', text: 'Two' },
    { value: '3', text: 'Three' },
]

export default function AppSurveyForm() {
    const classes = useStyles();
    const dispatch = useAuthDispatch();
    const {
        createSurvey: { surveyId, status, updateStatus, loading },
        surveys: { surveyData, toDefault, status: statusSurve, loading: loadingSurve },
        login: { token }
    } = useAuthState();
    const [selectedOption, setSelectedOption] = useState<string[]>([]);
    const [previewImage, setPreviewImage] = useState('');

    let survey: SurveyType = {
        id: null,
        name: '',
        description: '',
        language: '',
        transactionMode: 0,
        tag: '',
        template: {
            questions: [],
            logoImage: '',
            title: ''
        }
    };

    const formik = useFormik({
        initialValues: {
            surveyName: '',
            description: '',
            language: 'EN',
            transactionMode: '',
            logoImage: ''
        },
        validationSchema: getValidationFields(["surveyName", "description", "language", "transactionMode"]),
        onSubmit: (values) => {
            const logoImage_base64 = values.logoImage.substring(values.logoImage.indexOf(',') + 1);

            if (surveyData !== undefined && surveyData !== null) {
                survey = surveyData;
            }
            survey.name = values.surveyName;
            survey.description = values.description;
            survey.language = values.language;
            survey.transactionMode = parseInt(values.transactionMode);
            survey.tag = `"${selectedOption.join(",")}"`;
            if (survey.template === null) {
                survey.template = {
                    questions: [],
                    logoImage: logoImage_base64,
                    title: ""
                }
            } else {
                survey.template.logoImage = logoImage_base64 === "" ? survey.template.logoImage : logoImage_base64;
            }

            createSurvey(dispatch, survey, token)
        }
    })

    const handleOnImageUpload = (event: any) => {
        if (event.target.files && event.target.files[0]) {
            setPreviewImage(URL.createObjectURL(event.target.files[0]));
            blobToBase64(event.target.files[0]).then(base64 => {
                formik.setFieldValue('logoImage', base64)
            })
        }
    }

    // 
    useEffect(() => {
        if (surveyData !== null) {
            surveyData.name && formik.setFieldValue('surveyName', surveyData.name)
            surveyData.description && formik.setFieldValue('description', surveyData.description)
            surveyData.language && formik.setFieldValue('language', surveyData.language)
            surveyData.transactionMode && formik.setFieldValue('transactionMode', surveyData.transactionMode)
            surveyData.tag && setSelectedOption(JSON.parse(surveyData.tag).split(",")[0] === "" ? [] : JSON.parse(surveyData.tag).split(","));
            surveyData?.template?.logoImage && setPreviewImage(URL.createObjectURL(base64ToBlob(surveyData.template.logoImage)));
        }
    }, [surveyData])

    useEffect(() => {
        if (toDefault) {
            formik.setFieldValue('surveyName', '')
            formik.setFieldValue('description', '')
            formik.setFieldValue('language', 'EN')
            formik.setFieldValue('transactionMode', '')
        }
    }, [toDefault])


    const handleSelectChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setSelectedOption(event.target.value as string[]);
    };

    if (loadingSurve) return <AppSuspenseLoading />
    return <form onSubmit={formik.handleSubmit}>
        <Grid container spacing={3}>
            {
                updateStatus && <AppSnackbarAlert type="warning" text="snackbar_alert_updated" />
            }
            {
                status && <AppSnackbarAlert type="success" text="snackbar_alert_success" />
            }

            <Grid item xs={12} sm={4}>
                <AppInput className={classes.bottomSpacing}
                    label="Survey Name"
                    placeholder="Survey Name"
                    asterisk
                    name="surveyName"
                    value={formik.values.surveyName}
                    onChange={formik.handleChange}
                    error={formik.touched.surveyName && Boolean(formik.errors.surveyName)}
                    helperText={`${formik.touched.surveyName && formik.errors.surveyName}`}
                    fullWidth={true} />
                <AppSelect className={classes.bottomSpacing}
                    label="Language"
                    options={languageOptions}
                    name="language"
                    value={formik.values.language}
                    handleChange={formik.handleChange}
                    error={formik.touched.language && Boolean(formik.errors.language)}
                    helperText={`${formik.touched.language && formik.errors.language}`}
                    asterisk
                    fullWidth={true} />
                <AppMultipleSelect className={classes.bottomSpacing}
                    placeholder="Survey Tag"
                    label="Survey Tag"
                    options={tagsOptions}
                    selectedOption={selectedOption}
                    handleChange={handleSelectChange}
                    fullWidth={true} />
            </Grid>
            <Grid item xs={12} sm={4}>
                <AppInput className={classes.bottomSpacing}
                    label="Description"
                    placeholder="Description"
                    name="description"
                    value={formik.values.description}
                    onChange={formik.handleChange}
                    error={formik.touched.description && Boolean(formik.errors.description)}
                    helperText={`${formik.touched.description && formik.errors.description}`}
                    asterisk
                    fullWidth={true} />
                <AppSelect className={classes.bottomSpacing}
                    label="Transation Mode"
                    defaultValue="Transation Mode"
                    options={transactionModeOptions}
                    name="transactionMode"
                    value={formik.values.transactionMode}
                    handleChange={formik.handleChange}
                    error={formik.touched.transactionMode && Boolean(formik.errors.transactionMode)}
                    helperText={`${formik.touched.transactionMode && formik.errors.transactionMode}`}
                    asterisk
                    fullWidth={true} />
            </Grid>
            <Grid item xs={12} sm={4}>
                <AppUploadFiles previewImage={previewImage} handleOnImageUpload={handleOnImageUpload} />
            </Grid>
            <Grid item sm={12}>
                <AppFormButton text="SAVE" startIcon={<SaveIcon />} loading={loading} />
            </Grid>
        </Grid>
    </form>
}