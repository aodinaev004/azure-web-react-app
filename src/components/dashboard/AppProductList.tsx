import {
    Theme,
    createStyles,
    makeStyles
} from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "block",
            marginTop: theme.spacing(1)
        },
        details: {
            display: "flex",
            flexDirection: "column",
        },
        content: {
            flex: "1 0 auto",
        },
        cover: {
            width: 151,
        },
        controls: {
            display: "flex",
            alignItems: "center",
            paddingLeft: theme.spacing(1),
            paddingBottom: theme.spacing(1),
        },
        playIcon: {
            height: 38,
            width: 38,
        },
    })
);

interface ProductListType {
    title: string;
    questionCount: number;
    duration: number;
}

export default function AppProductList({
    title,
    questionCount,
    duration,
}: ProductListType) {
    const classes = useStyles();

    return (
        <Card className={classes.root} style={{ width: "100%" }}>
            <div className={classes.details}>
                <Grid
                    container
                    direction="row"
                    justifyContent="space-between"
                    alignItems="center"
                    spacing={3}
                >
                    <Grid item>
                        <CardContent className={classes.content}>
                            <Typography
                                component="h6"
                                variant="h6"
                                style={{ fontSize: "1rem" }}
                            >
                                {title}
                            </Typography>
                            <Typography variant="body2" color="textSecondary">
                                {`${questionCount} questions | ${duration} mins`}
                            </Typography>
                        </CardContent>
                    </Grid>
                </Grid>
            </div>
        </Card>
    );
}
