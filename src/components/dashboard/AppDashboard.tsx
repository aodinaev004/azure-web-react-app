import React, { useState } from 'react';
import { Hidden } from '@material-ui/core';
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles'
import { Box, Container } from '@material-ui/core';

import AppBottomNavBar from '../../components/dashboard/AppBottomNavBar';
import AppSidebar from "../../components/dashboard/AppSidebar";
import AppSidebarMobile from '../../components/dashboard/AppSidebarMobile';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            height: '100%',
            overflow: 'hidden',
            width: '100%'
        },
        wrapper: {
            display: 'flex',
            flex: '1 1 auto',
            overflow: 'hidden',
            paddingTop: 64,
            height: "calc(100vh - 64px)",
            [theme.breakpoints.down('xs')]: {
                height: "calc(100vh - 120px)",
            }
        },
        container: {
            display: 'flex',
            flex: '1 1 auto',
            overflow: 'hidden'
        },
        content: {
            flex: '1 1 auto',
            height: '100%',
            overflow: 'auto'
        }
    })
)

const AppDashboard = ({children} : {children:any}) => {
    const classes = useStyles();
    const [value, setValue] = useState('menu');
    const [open, setOpen] = useState(false);

    const handleBottomMenuOpen = (event: React.ChangeEvent<{}>, newValue: string) => {
        setValue(newValue);
        setOpen(!open);
    };

    const onMobileClose = () => {
        setOpen(false);
    }

    return (
        <div>
            {/* <AppHeader /> */}
            <div className={classes.wrapper}>
                <Hidden smDown>
                    <AppSidebar />
                </Hidden>
                <Hidden smUp>
                    <AppSidebarMobile open={open} onMobileClose={onMobileClose} />
                </Hidden>
                <div className={classes.container}>
                    <div className={classes.content}>
                    <Box
                        sx={{
                            // backgroundColor: 'background.default',
                            height: 'calc(100vh - 115px)',
                            py: 3
                        }}>
                        <Container maxWidth={false}>
                           {children}
                        </Container>
                    </Box>
                    </div>
                </div>
            </div>
            <Hidden smUp>
                <AppBottomNavBar value={value} handleBottomMenuOpen={handleBottomMenuOpen} />
            </Hidden>
            
        </div>
    );
}

export default AppDashboard;