import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { FormControl, FormHelperText } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        textfield: {
            width: '100%',
        },
    }),
);

export default function AppTextField({ name, helperText, label, rows, value, handleChange, defaultValue, error, fullWidth }: { name?: string, helperText?: string, label?: string, rows: number, value?: string, handleChange?: (e: React.ChangeEvent<any>) => void, defaultValue?: string, error?: boolean, fullWidth?: boolean }) {
    const classes = useStyles();
    const { t } = useTranslation();

    return (
        <FormControl fullWidth={fullWidth} error={error}>
            <TextField
                id="outlined-multiline-static"
                label={label}
                multiline
                rows={rows}
                defaultValue={defaultValue}
                variant="outlined"
                className={classes.textfield}
                value={value}
                onChange={handleChange}
                name={name}
            />
            {error && <FormHelperText>{t(`${helperText}`)}</FormHelperText>}
        </FormControl>
    );
}
