import Button from "@material-ui/core/Button";
import CircularProgress from '@material-ui/core/CircularProgress';

const btnStyle = {
    paddingTop: "12px",
    paddingLeft: "39px",
    paddingBottom: "12px",
    paddingRight: "39px",
    fontSize: "13px",
}

interface ButtonT {
    text: string;
    classname?: string;
    onClick?: any;
    loading?: boolean;
    startIcon?: React.ReactNode;
    endIcon?: React.ReactNode;
}

const AppFormButton = ({ text, classname, onClick, loading, startIcon, endIcon }: ButtonT) => {
    return <Button
        type="submit"
        disabled={loading}
        variant="contained"
        color="primary"
        style={btnStyle}
        className={classname}
        onClick={onClick}
        startIcon={startIcon && startIcon}
        endIcon={endIcon && endIcon}
    >
        {
            loading ? <CircularProgress color="inherit" size="1.5rem" /> : text
        }
    </Button>
}

export default AppFormButton;