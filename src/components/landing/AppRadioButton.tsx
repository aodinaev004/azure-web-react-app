import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import colors from '../../config/colors';

interface RadioProperties {
    row?: boolean;
    name?: string;
    groupLabel?: string;
    defaultValue?: string | number | readonly string[] | undefined;
    value?: string;
    labelPlacement?: "top" | "end" | "start" | "bottom" | undefined
    options?: string[];
    handleChange?: (e: React.ChangeEvent<any>) => void;
}

export default function AppRadioButton({ row = false, name, groupLabel, defaultValue = "top", labelPlacement, value, handleChange, options }: RadioProperties) {
    return (
        <FormControl component="fieldset">
            {groupLabel && <FormLabel component="legend">{groupLabel}</FormLabel>}
            <RadioGroup row={row} aria-label="position" name={name} value={value} onChange={handleChange}>
                {(options as string[]).map(option => {
                    return <FormControlLabel key={option}
                        value={option}
                        control={<Radio style={{ color: colors.primary }} />}
                        label={option}
                        labelPlacement={labelPlacement}
                    />
                })}
            </RadioGroup>
        </FormControl>
    );
}
