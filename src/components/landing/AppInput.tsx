import Input from "@material-ui/core/Input";
import FormControl from "@material-ui/core/FormControl";
import InputAdornment from "@material-ui/core/InputAdornment";
import { FormHelperText, InputLabel } from "@material-ui/core";
import { useTranslation } from 'react-i18next';
import colors from "../../config/colors";
import { FocusEventHandler } from "react";

interface ChildType {
    label?: string;
    asterisk?: boolean;
    error?: boolean;
    helperText?: string;
    name?: string;
    icon?: React.ReactNode;
    placeholder?: string;
    type?: string;
    value?: string;
    disabled?: boolean;
    style?: { [key: string]: string };
    className?: string;
    onChange?: (e: React.ChangeEvent<any>) => void;
    onBlur?: FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>,
    fullWidth?: boolean;
    inputReference?: React.RefObject<HTMLInputElement>;
}
const asteriskStyle = {
    color: 'red',
    fontSize: 22
}
const AppInput = ({ label, asterisk, placeholder, icon, type = "text", value, onChange, onBlur, name, error, helperText, disabled, style, className, fullWidth = false, inputReference }: ChildType) => {
    const { t } = useTranslation();

    return <FormControl fullWidth={fullWidth}
        error={error}
    >
        {label &&
            <InputLabel shrink htmlFor={`input-id-${label?.toLowerCase()}`} style={{ color: `${colors.primary}` }} >
                {label}
                {asterisk && <span style={asteriskStyle}>  *</span>}
            </InputLabel>
        }
        <Input
            ref={inputReference}
            id={label && `input-id-${label?.toLowerCase()}`}
            disabled={disabled}
            name={name}
            placeholder={placeholder}
            type={type}
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            startAdornment={
                icon &&
                <InputAdornment position="start">
                    {icon}
                </InputAdornment>
            }
            className={className}
            style={style}
        />
        {error && <FormHelperText>{t(`${helperText}`)}</FormHelperText>}
    </FormControl >
}

export default AppInput;