import { createStyles, makeStyles, Theme } from "@material-ui/core";
import { Collapse, List, ListItem, ListItemText } from "@material-ui/core"
import ExpandLess from "@material-ui/icons/ExpandLess"
import ExpandMore from "@material-ui/icons/ExpandMore"
import { useState } from "react"

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        list: {
            width: 250,
        },
        fullList: {
            width: "auto",
        },
        menuBtn: {
            display: "flex",
            justifyContent: "space-evenly",
            [theme.breakpoints.up("md")]: {
                display: "none",
            },
        },
        listItem: {
            marginLeft: theme.spacing(2),
        },
        nested: {
            paddingLeft: theme.spacing(7),
        },
    })
);

interface SelectOptionsType {
    defaultValue: string;
    options: { value: string, text: string }[];
    name?: string;
    value: string;
    error?: boolean;
    helperText?: string;
    handleChange: (e: React.ChangeEvent<any>) => void;
    fullWidth?: boolean;
}

const AppDropDown = () => {
    const [open, setOpen] = useState(false);
    const classes = useStyles();

    return <List>
        <ListItem button onClick={() => setOpen(!open)}>
            <ListItemText
                className={classes.listItem}
                primary=""
            />
            {open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
                <ListItem button className={classes.nested}>
                    <ListItemText primary="Link 1" />
                </ListItem>
                <ListItem button className={classes.nested}>
                    <ListItemText primary="Link 2" />
                </ListItem>
                <ListItem button className={classes.nested}>
                    <ListItemText primary="Link 3" />
                </ListItem>
            </List>
        </Collapse>
    </List>
}

export default AppDropDown;