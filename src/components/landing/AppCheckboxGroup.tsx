import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { FormHelperText } from "@material-ui/core";
import colors from "../../config/colors";
import { useTranslation } from "react-i18next";

const AppCheckboxGroup = ({ checked = false, label, className, name, error, helperText, handleChange, checkboxOptions }:
    {
        checked?: boolean, label?: any, className?: string, name?: string, error?: boolean, helperText?: string,
        handleChange?: (e: React.ChangeEvent<any>) => void, checkboxOptions: any
    }) => {
        const { t } = useTranslation();

    return <FormControl error={error}>
        <FormGroup row>
            {checkboxOptions && (checkboxOptions as string[]).map(option => {
            <FormControlLabel
                    className={className}
                    name={name}
                    control={<Checkbox
                        style={{ color: colors.primary }}
                        checked={checked}
                        onChange={handleChange}
                    />}
                    label={label}
                />}
            )}
        </FormGroup>
        {error && <FormHelperText>{t(`${helperText}`)}</FormHelperText>}
    </FormControl>
}

export default AppCheckboxGroup;