/* eslint-disable no-console */
// @ts-nocheck
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import { Formik, Form, Field, FieldProps } from "formik";
import { format, isWeekend } from "date-fns";
import { KeyboardDateTimePicker, DateTimePickerProps } from "@material-ui/pickers";

interface DatePickerFieldProps extends FieldProps, DateTimePickerProps {
    getShouldDisableDateError: (date: Date) => string;
}

function DatePickerField({
    form,
    field: { value, name },
    maxDate = new Date("2099-12-31"),
    minDate = new Date("1900-01-01"),
    getShouldDisableDateError,
    ...other
}: DatePickerFieldProps) {
    const currentError = form.errors[name];
    const toShowError = Boolean(currentError && form.touched[name]);

    return (
        <KeyboardDateTimePicker
            clearable
            ampm={true}
            minDate={minDate}
            maxDate={maxDate}
            value={value}
            format="MM/dd/yyyy HH:mm a"
            onError={(reason, value) => {
                switch (reason) {
                    case "invalidDate":
                        form.setFieldError(name, "Invalid date format");
                        break;

                    case "disablePast":
                        form.setFieldError(name, "Values in the past are not allowed");
                        break;

                    case "maxDate":
                        form.setFieldError(name, `Date should not be after ${format(maxDate, "P")}`);
                        break;

                    case "minDate":
                        form.setFieldError(name, `Date should not be before ${format(minDate, "P")}`);
                        break;

                    case "shouldDisableDate":
                        // shouldDisableDate returned true, render custom message according to the `shouldDisableDate` logic
                        form.setFieldError(name, getShouldDisableDateError(value));
                        break;

                    default:
                        form.setErrors({
                            ...form.errors,
                            [name]: undefined,
                        });
                }
            }}
            // Make sure that your 3d param is set to `false` on order to not clear errors
            onChange={(date) => form.setFieldValue(name, date, false)}
            renderInput={(props) => (
                <TextField
                    {...props}
                    name={name}
                    error={toShowError}
                    helperText={toShowError ? currentError ?? props.helperText : undefined}
                    // Make sure that your 3d param is set to `false` on order to not clear errors
                    onBlur={() => form.setFieldTouched(name, true, false)}
                />
            )}
            {...other}
        />
    );
}

function validateDatePickerValue(date: Date) {
    if (isWeekend(date)) {
        return "Weekends are not allowed";
    }

    return null;
}

export default function FormikDateTimePicker() {
    return (
        <Formik onSubmit={console.log} initialValues={{ date: new Date() }}>
            {({ values, errors }) => (
                <Form>
                    <Grid container>
                        <Grid item container justifyContent="flex-start" xs={12}>
                            <Field
                                name="date"
                                disablePast
                                component={DatePickerField}
                                shouldDisableDate={(date: Date) => validateDatePickerValue(date) !== null}
                                getShouldDisableDateError={validateDatePickerValue}
                            />
                        </Grid>
                    </Grid>
                </Form>
            )}
        </Formik>
    );
}
