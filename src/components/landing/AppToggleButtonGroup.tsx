import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  Hidden,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { ToggleButton, ToggleButtonGroup } from "@material-ui/lab";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";

import colors from "../../config/colors";

interface ButtonGroupType {
  min: number;
  max: number;
  step: number;
  name?: string;
  value?: string;
  handleChange?: (e: React.MouseEvent<HTMLElement>, newValue: string) => void;
  helperText?: string;
  error?: boolean;
  fullWidth?: boolean;
}

export default function AppToggleButtonGroup({
  min,
  max,
  step,
  name,
  value,
  handleChange,
  helperText,
  error,
  fullWidth,
}: ButtonGroupType) {
  const elements = [];
  const { t } = useTranslation();

  for (let i = min; i <= max; i += step) {
    elements.push(<ToggleButton id={`btn${i}`} value={`${i}`} style={{width:40}}>{i}</ToggleButton>);
  }

  return (
    <div>
      <ToggleButtonGroup
        size="medium"
        value={value}
        exclusive
        onChange={handleChange}
        id={name}
      >
        {elements}
      </ToggleButtonGroup>
    </div>
  );
}
