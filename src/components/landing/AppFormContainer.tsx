import React from 'react';
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginTop: 100,
    },
  }),
);

const AppFormContainer = ( { children } : { children: React.ReactNode }) => {
  const classes = useStyles();

  return (
    <Grid className={"sign-in "+classes.root}
        container
        spacing={0}
        direction="column"
        justifyContent="center">
        <Grid className="container">
            {children}
        </Grid>
    </Grid>
  );
}
export default  AppFormContainer;