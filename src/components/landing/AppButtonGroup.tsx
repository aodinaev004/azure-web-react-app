import { Checkbox, FormControl, FormControlLabel, FormGroup, FormHelperText, Hidden } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import React, { useState } from 'react';
import { useTranslation } from "react-i18next";

import colors from '../../config/colors';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column',
            '& > *': {
                margin: theme.spacing(1),
            },
            "& .MuiButton-outlinedPrimary": {
                color: colors.primary,
            }
        },
    }),
);

interface ButtonGroupType {
    min: number;
    max: number;
    step: number;
    name?: string;
    value?: string;
    handleChange?: (e: React.ChangeEvent<any>) => void;
    helperText?: string;
    error?: boolean;
    fullWidth?: boolean;
}

export default function AppButtonGroup({ min, max, step, name, value, handleChange, helperText, error, fullWidth }: ButtonGroupType) {
    const classes = useStyles();
    const elements = [];
    const [checked, setValue] = useState(false);
    const { t } = useTranslation();

    const handleClick = (e: any) => {
        setValue(true);
    }

    for (let i = min; i <= max; i += step) {
        elements.push(<Button onClick={handleClick}>{i}</Button>);
    }

    return (
        <div className={classes.root}>
            <ButtonGroup color="primary" aria-label="outlined primary button group">
                {elements}
            </ButtonGroup>
            <Hidden smUp smDown>
                <FormControl fullWidth={fullWidth} error={error}>
                    <FormGroup>
                        <FormControlLabel
                            name={name}
                            control={<Checkbox
                                style={{ color: colors.primary }}
                                checked={checked}
                                onChange={handleChange}
                            />}
                            label="sdf"
                        />
                    </FormGroup>

                    {error && <FormHelperText>{t(`${helperText}`)}</FormHelperText>}
                </FormControl>
            </Hidden>
        </div>
    );
}