import { Redirect, Route } from "react-router-dom";

import { useEffect } from "react";
import { useAuthState, useAuthDispatch } from '../../context';
import { RouteType } from '../../config/types';
import AppDashboard from "../dashboard/AppDashboard";

const AppRoutes = ({ component, path, isPrivate, ...rest }: RouteType) => {

  const dispatch = useAuthDispatch();
  const { login: { token, auth } } = useAuthState()
  const ChildComponent = component;

  useEffect(() => {
    if (auth && token === '') dispatch({ type: 'LOGOUT' });
  }, [auth, token, dispatch]);

  return (
    <Route
      path={path}
      render={props =>
        isPrivate && token === '' ? (
          <Redirect
            push
            to={{ pathname: "/" }}
          />
        )
          : isPrivate && token !== '' ?
            (
              <AppDashboard children={<ChildComponent {...props} />} />
            )
            : !isPrivate && token !== '' ?
              (
                <Redirect
                  push
                  to={{ pathname: "/dashboard" }}
                />
              )
              :
              (
                <ChildComponent {...props} />
              )
      }
      {...rest}
      
    />
  )
}

export default AppRoutes