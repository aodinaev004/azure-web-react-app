import React, { useState } from "react";
import clsx from "clsx";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import MenuIcon from "@material-ui/icons/Menu";
import IconButton from "@material-ui/core/IconButton";
import colors from "../../config/colors";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import Collapse from "@material-ui/core/Collapse";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    list: {
      width: 250,
    },
    fullList: {
      width: "auto",
    },
    menuBtn: {
      display: "flex",
      justifyContent: "space-evenly",
      [theme.breakpoints.up("md")]: {
        display: "none",
      },
    },
    listItem: {
      marginLeft: theme.spacing(2),
    },
    nested: {
      paddingLeft: theme.spacing(7),
    },
  })
);

type Anchor = "top" | "left" | "bottom" | "right";

export default function AppRightMenu() {
  const { t } = useTranslation();
  const classes = useStyles();
  let history = useHistory();
  const [open, setOpen] = useState(false);
  const [state, setState] = useState({
    right: false,
  });

  const toggleDrawer =
    (anchor: Anchor, open: boolean) =>
      (event: React.KeyboardEvent | React.MouseEvent) => {
        setOpen(false);
        if (
          event.type === "keydown" &&
          ((event as React.KeyboardEvent).key === "Tab" ||
            (event as React.KeyboardEvent).key === "Shift")
        ) {
          return;
        }

        setState({ ...state, [anchor]: open });
      };

  const list = (anchor: Anchor) => (
    <div>
      <List style={{ paddingBottom: "0" }}>
        <ListItem button onClick={() => setOpen(!open)}>
          <ListItemText
            className={classes.listItem}
            primary={t("nav_Solution")}
          />
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItem button className={classes.nested}>
              <ListItemText primary="Link 1" />
            </ListItem>
            <ListItem button className={classes.nested}>
              <ListItemText primary="Link 2" />
            </ListItem>
            <ListItem button className={classes.nested}>
              <ListItemText primary="Link 3" />
            </ListItem>
          </List>
        </Collapse>
      </List>
      <div
        className={clsx(classes.list, {
          [classes.fullList]: anchor === "top" || anchor === "bottom",
        })}
        role="presentation"
        onClick={toggleDrawer(anchor, false)}
        onKeyDown={toggleDrawer(anchor, false)}
      >
        <List style={{ paddingTop: "0" }}>
          <ListItem button>
            <ListItemText
              className={classes.listItem}
              primary={t("nav_PlanPrice")}
            />
          </ListItem>
          <ListItem button>
            <ListItemText
              className={classes.listItem}
              primary={t("nav_Contact")}
            />
          </ListItem>
        </List>
        <Divider />
        <List>
          <ListItem button>
            <ListItemText
              className={classes.listItem}
              primary={t("nav_RequestDemo")}
            />
          </ListItem>
          <ListItem button onClick={() => history.push("/signin")}>
            <ListItemText
              className={classes.listItem}
              primary={t("form_SignIn")}
            />
          </ListItem>
        </List>
        <Button
          variant="contained"
          color="secondary"
          style={{
            backgroundColor: colors.secondary,
            display: "block",
            marginLeft: "auto",
            marginRight: "auto",
          }}
          onClick={() => history.push("/signup")}
        >
          {t("nav_SignUp")}
        </Button>
      </div>
    </div>
  );

  return (
    <div>
      <React.Fragment>
        <IconButton
          onClick={toggleDrawer("right", true)}
          className={classes.menuBtn}
          edge="start"
          color="inherit"
          aria-label="menu"
        >
          <MenuIcon color="primary" />
        </IconButton>
        <Drawer
          anchor="right"
          open={state["right"]}
          onClose={toggleDrawer("right", false)}
        >
          {list("right")}
        </Drawer>
      </React.Fragment>
    </div>
  );
}
