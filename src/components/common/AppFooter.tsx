import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import { useTranslation, Trans } from 'react-i18next';
import { useAuthState, useAuthDispatch } from '../../context';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from "@material-ui/core/MenuItem";

import colors from '../../config/colors';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    position: 'absolute',
    marginTop: 65
  },
  link: {
    color: colors.lightBlue,
  },
  formControl: {
    margin: 0,
    minWidth: 55,
  },
  select: {
    color: colors.lightBlue,
    '&.MuiInput-underline:before': {
      borderBottom: 'none'
    },
    '&.MuiInput-underline:hover:not(.Mui-disabled):before': {
      borderBottom: 'none'
    },
    '&..MuiInput-underline:after': {
      borderBottom: 'none'
    },
  },
  icon: {
    fill: colors.lightBlue,
  },
  midGrid: {
    borderLeft: `1px solid ${colors.gray}`,
    borderRight: `1px solid ${colors.gray}`,
    maxWidth: 120
  },
  rightGrid: {
    maxWidth: 120
  }
}));


const AppFooter = () => {
  const classes = useStyles();
  const { t, i18n } = useTranslation();
  const dispatch = useAuthDispatch();
  const { 
    login: { auth },
    language: { lang }
  } = useAuthState();

  const handleChange = (event: any) => {
    let language = event.target.value;
    dispatch({type: "LANG_CHANGE", payload:{ language}})
    i18n.changeLanguage(language)
   
  };

  if (auth) return <></>;
  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="center"
      className={classes.root}
    >
      <Grid item xs={12} sm={4}  >
        <Trans i18nKey="footer_Copyight">
          <strong title={t('footer_Copyight')}></strong>
        </Trans>
        {/* <b>Copyright &copy; 2021</b> All rights reserved. Privacy & Leagal Policies */}
      </Grid>
      <Grid item xs={12} sm={3} className={classes.midGrid}>
        <Link href="#" className={classes.link}>{t('footer_Support')} </Link>
      </Grid>
      <Grid item xs={12} sm={3} className={classes.midGrid}>
        <FormControl className={classes.formControl}>
          <Select
            value={lang}
            onChange={handleChange}
            className={classes.select}
            inputProps={{
              classes: {
                icon: classes.icon,
              },
            }}
          >
            <MenuItem value="en">English</MenuItem>
            <MenuItem value="fr">French</MenuItem>
          </Select>
        </FormControl>
      </Grid>
    </Grid>
  );
}

export default AppFooter;