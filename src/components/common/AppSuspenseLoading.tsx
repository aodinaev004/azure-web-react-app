import { Theme } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import withStyles from '@material-ui/core/styles/withStyles';
import createStyles from '@material-ui/core/styles/createStyles';

const BorderLinearProgress = withStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      height: 2,
    },
    colorPrimary: {
      backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
    },
    bar: {
      borderRadius: 5,
      backgroundColor: '#009FE3',
    },
  }),
)(LinearProgress);

export default function AppSuspenseLoading() {
  return (
    <BorderLinearProgress  />
  );
}