import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import ListAltOutlinedIcon from "@material-ui/icons/ListAltOutlined";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import { Badge, Button, Divider, Grid, Link } from "@material-ui/core";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { useTranslation } from "react-i18next";
import { Link as RouteLink } from "react-router-dom";
import { useHistory } from "react-router-dom";

import { useAuthDispatch, useAuthState } from '../../context';
import AppLogo from '../../assets/images/logo192.png';
import colors from "../../config/colors";
import AppRightMenu from "./AppRightMenu";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    logo: {
      marginRight: theme.spacing(2),
      width: "50px",
      height: "50px",
    },
    title: {
      flexGrow: 2,
      display: "flex",
      justifyContent: "flex-end",
    },
    link: {
      marginRight: theme.spacing(2),
      color: colors.primary,
      display: "inline-flex",
      alignItems: "center",
    },
    navbar: {
      backgroundColor: "#fff",
    },
    menu: {
      marginTop: theme.spacing(6),
      "& .MuiMenu-paper": {
        width: "250px",
      },
    },
    badge: {
      "& .MuiBadge-anchorOriginTopRightRectangle": {
        right: "auto",
      },
    },
    topMenu: {
      display: "contents",
      [theme.breakpoints.down("sm")]: {
        display: "none",
      },
    },
    rightMenu: {
      marginLeft: "auto",
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
    menuGrid: {
      justifyContent: "space-between",
      [theme.breakpoints.down("sm")]: {
        justifyContent: "flex-end",
      },
    },
    formControl: {
      margin: 0,
      minWidth: 55,
    },
    select: {
      color: colors.lightBlue,
      '&.MuiInput-underline:before': {
        borderBottom: 'none'
      },
      '&.MuiInput-underline:hover:not(.Mui-disabled):before': {
        borderBottom: 'none'
      },
      '&..MuiInput-underline:after': {
        borderBottom: 'none'
      },
    },
    icon: {
      fill: colors.lightBlue,
    },
  })
);

export default function AppHeader() {
  const classes = useStyles();
  const { t } = useTranslation();
  let history = useHistory();
  const dispatch = useAuthDispatch();
  const {
    login: {
      userDetails: { email, username },
      auth
    }
  } = useAuthState();
  const [userMenuEl, setUserMenuEl] = React.useState<null | HTMLElement>(null);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const defaultProps = {
    color: "secondary" as "secondary",
  };

  const handleCreateCustomSurvey = (e: any) => {

    e.preventDefault()
    dispatch({ type: "GET_SURVEY_DEFAULT" })
    dispatch({ type: "CREATE_SURVEY_DEFAULT" })
    history.push('/dashboard/create-custom-survey')
  };

  const handleUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setUserMenuEl(event.currentTarget);
  };

  const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleUserClose = () => {
    setUserMenuEl(null);
  };

  const signoutClick = () => {
    dispatch({ type: 'LOGOUT' })
    setUserMenuEl(null);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <AppBar
        className={classes.navbar}>
        <Toolbar>
        {auth ? <RouteLink to="/dashboard"><img src={AppLogo} className={classes.logo} alt="" /></RouteLink>
              : <RouteLink to="/"><img src={AppLogo} className={classes.logo} alt="" /></RouteLink>
        }
          <div className={classes.root}>
            <Grid
              container
              spacing={3}
              direction="row"
              className={classes.menuGrid}
              alignItems="center"
            >
              <Grid item className={classes.topMenu}>
                <Typography variant="overline">
                  <Link
                    href="#"
                    aria-controls="simple-menu"
                    className={classes.link}
                    aria-haspopup="true"
                    onClick={handleMenu}
                  >
                    {t("nav_Solution")}
                    <ArrowDropDownIcon fontSize="small" />
                  </Link>
                  <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                    className={classes.menu}
                  >
                    <MenuItem onClick={handleClose}>Link 1</MenuItem>
                    <MenuItem onClick={handleClose}>Link 2</MenuItem>
                    <MenuItem onClick={handleClose}>Link 3</MenuItem>
                  </Menu>
                  <Link href="#" className={classes.link}>
                    {t("nav_PlanPrice")}
                  </Link>
                  <Link href="#" className={classes.link}>
                    {t("nav_Contact")}
                  </Link>
                </Typography>
              </Grid>

              {auth && (<>
                <Grid item className={classes.topMenu}>
                  <Typography variant="overline">
                    <Link
                      href="#"
                      className={classes.link}
                      style={{ display: "flex" }}
                    >
                      <ListAltOutlinedIcon style={{ color: colors.primary }} />
                      {t('dash_CreateFromTemplateBtn')}
                    </Link>
                  </Typography>
                </Grid>
                <Grid item className={classes.topMenu}>
                  <Typography variant="overline">
                    <Link
                      href=""
                      onClick={handleCreateCustomSurvey}
                      className={classes.link}
                      style={{ display: "flex" }}
                    >
                      <ListAltOutlinedIcon style={{ color: colors.primary }} />
                      {t("nav_CreateCustomSurvey")}
                    </Link>
                  </Typography>
                </Grid>
              </>)}

              <Grid item className={classes.topMenu}>
                {!auth && (
                  <div>
                    <Typography variant="overline">
                      <Link href="#" className={classes.link}>
                        {t("nav_RequestDemo")}
                      </Link>
                      <RouteLink to="/signin" className={classes.link}>
                        {t("form_SignIn")}
                      </RouteLink>
                      <Button
                        variant="contained"
                        color="secondary"
                        style={{ backgroundColor: colors.secondary }}
                        onClick={() => history.push("/signup")}
                      >
                        {t("nav_SignUp")}
                      </Button>
                    </Typography>
                  </div>
                )}
              </Grid>

              {auth && (
                <Grid item>
                  <IconButton
                    aria-label="account of current user"
                    aria-controls="menu-appbar"
                    aria-haspopup="true"
                    onClick={handleUserMenu}
                    color="secondary"
                  >
                    <AccountCircle />
                  </IconButton>
                  <Menu
                    id="menu-appbar"
                    className={classes.menu}
                    anchorEl={userMenuEl}
                    anchorOrigin={{
                      vertical: "top",
                      horizontal: "right",
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: "top",
                      horizontal: "right",
                    }}
                    open={Boolean(userMenuEl)}
                    onClose={handleUserClose}
                  >
                    <MenuItem onClick={handleUserClose}>
                      <Grid
                        container
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                      >
                        <Grid item xs={8}>
                          <Typography>{username}</Typography>
                        </Grid>
                        <Grid item xs={4}>
                          <Badge
                            className={classes.badge}
                            badgeContent={"BASIC"}
                            {...defaultProps}
                          />
                        </Grid>
                      </Grid>
                    </MenuItem>
                    <MenuItem onClick={handleUserClose}>
                      {email}
                    </MenuItem>
                    <Divider />
                    <MenuItem onClick={signoutClick}>
                      {t("nav_SignOut")}
                    </MenuItem>
                  </Menu>
                </Grid>
              )}
            </Grid>
          </div>
          <div className={classes.rightMenu}>
            <AppRightMenu />
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
