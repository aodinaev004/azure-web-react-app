import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import Backend from 'i18next-http-backend';

let defLang = localStorage.getItem("lang") || "en";

i18n
  .use(Backend)
  .use(initReactI18next)
  .init({
    lng: defLang,
    backend: {
      /* translation file path */
      loadPath: '/{{ns}}/{{lng}}.json'
    },
    fallbackLng: defLang,
    debug: false,
    /* can have multiple namespace, in case you want to divide a huge translation into smaller pieces and load them on demand */
    ns: ['translations'],
    defaultNS: 'translations',
    keySeparator: false,
    interpolation: {
      escapeValue: false,
      formatSeparator: ','
    },
    react: {
      wait: false,
      useSuspense: false,
    }
  });

export default i18n;