
interface ColorT {
  [id: string]: string;
}

const colors: ColorT = {
  primary: "#6dabe4",
  secondary: "#f50057",
  black: "#000",
  white: "#fff",
  gray: "#d1d1d1",
  lightBlue: "#4285f4",
  active: "#d2f4ff",
  scroll: "#6dabe4",
  blue: "#1976d2",
  error: "#f44336",
  draft: "#ff9800",
  proccessing: "#2196f3",
  success: "#4caf50",
  warning: "#ff9800",
  tooltip: "#757575"
};

export default colors;