export interface RouteType {
  component: any;
  path: string;
  isPrivate: boolean;
  exact?: boolean;
  rest?: string;
}

export interface AllSurveyT {
  id: number,
  name: string,
  description: string,
  status: string,
  createdAt: string,
  editedAt: string,
  language: string,
  rootId: number,
  numberOfQuestions: number,
}

export interface SurveyTotalCountT {
  icon?: React.ReactNode;
  title: string;
  total: number;
}

export interface QuestionType {
  id: number | null;
  type: string;
  title: string;
  description: string;
  required: boolean;
  orderPosition: number;
  body?: any;
  uid?: string;
  logic: string | null;
}

export interface SurveyType {
  id: number | null,
  name: string,
  createdAt?: string,
  description: string,
  editedAt?: string,
  language: string,
  numberOfQuestions?: number,
  rootId?: number,
  status?: string,
  tag?: string,
  transactionMode: number,
  type?: string
  template: {
    colorSchema?: string,
    id?: number | null,
    logoId?: number,
    logoImage: string,
    questions?: QuestionType[],
    title?: string
  },
}

export interface NewObject {
    [key: string]: any
}
