import { lazy } from 'react';
import { RouteType } from './types';
const NotFound = lazy(() => import('../pages/landing/NotFound'));
const Main = lazy(() => import('../pages/landing/Main'));
const SignIn = lazy(() => import('../pages/landing/SignIn'));
const SignUp = lazy(() => import('../pages/landing/SignUp'));
const Forgot = lazy(() => import('../pages/landing/Forgot'));
const Dashboard = lazy(() => import('../pages/dashboard/index'));
const CreateCustomnSurvey = lazy(() => import('../pages/dashboard/CreateCustomnSurvey'));

const routes: RouteType[] = [
  // -----------------LANDING ROUTES -------------------
  {
    path: '/',
    exact: true,
    isPrivate: false,
    component: Main
  },
  {
    path: '/signin',
    exact: true,
    isPrivate: false,
    component: SignIn
  },
  {
    path: '/signup',
    exact: true,
    isPrivate: false,
    component: SignUp
  },
  {
    path: '/forgot',
    exact: true,
    isPrivate: false,
    component: Forgot
  },
  // -----------------DASHBOARD ROUTES -------------------
  {
    path: '/dashboard',
    exact: true,
    isPrivate: true,
    component: Dashboard,
  },
  {
    path: `/dashboard/create-custom-survey`,
    exact: false,
    isPrivate: true,
    component: CreateCustomnSurvey
  },
  {
    path: `/dashboard/test`,
    exact: false,
    isPrivate: true,
    component: CreateCustomnSurvey
  },

  {
    path: '/*',
    isPrivate: false,
    component: NotFound
  },
]

export default routes;