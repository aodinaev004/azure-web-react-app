import { QuestionType, SurveyType } from "../../config/types";

export function blobToBase64(blob: Blob) {
  return new Promise((resolve, _) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result);
    reader.readAsDataURL(blob);
  })
}

export function base64ToBlob(dataURI: string) {
  dataURI = "data:image/jpeg;base64," + dataURI;

  let byteString = atob(dataURI.split(',')[1]);

  let ab = new ArrayBuffer(byteString.length);
  let ia = new Uint8Array(ab);

  for (let i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }
  return new Blob([ab], { type: 'image/jpeg' });
}

// convert date to DD-MM-YYYY HH:MM AM/PM
export function formatDate(date: string) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear(),
    hours = d.getHours(),
    min = d.getMinutes(),
    ampm = hours >= 12 ? 'PM' : 'AM',
    formated_date;

  if (month.length < 2)
    month = '0' + month;
  if (day.length < 2)
    day = '0' + day;

  formated_date = `${[day, month, year].join('-')} ${[hours, min].join(':')} ${ampm}`;
  return formated_date;
}

export function getNumberOrStarType(survey: SurveyType, fromApi?:boolean) {
  let questions = survey.template.questions;
    
  if (fromApi){
    questions = (questions as QuestionType[]).map(question => {
      const body = JSON.parse(question.body);
      if (question.type.toLowerCase() === "rating" && body.type === "star") {
        question.type = "STAR_RATING";
        return question;
      } else {
        return question;
      }
    })
  } else {
    questions = (questions as QuestionType[]).map(question => {
      const body = JSON.parse(question.body);
      if (question.type.toLowerCase() === "star_rating") {
        question.type = "RATING";
        return question;
      } else {
        return question;
      }
    })
  }
  
  survey.template.questions = questions;
  return survey;
}