import * as yup from 'yup'

let validationSchame = yup.object({
    name: yup.string()
        .required('validate_name_required'),
    email: yup.string()
        .email("validate_email_valid")
        .required('validate_email_required'),
    password: yup.string()
        .min(8, "validate_pass_min")
        .required('validate_pass_required'),
    pesswordConfirm: yup.string()
        .oneOf([yup.ref('password'), null], 'validate_pass_confirm_match')
        .required('validate_pass_confirm_required'),
    company: yup.string()
        .required('validate_company_required'),
    mobile: yup.string()
        .matches(/^[0-9]+$/, "validate_mobile_must_digits")
        .min(10, 'validate_mobile_min_10')
        .max(10, 'validate_mobile_max_10')
        .required('validate_mobile_required'),
    customerType: yup.string()
        .required('validate_customer_type_required'),
    agree: yup.boolean()
        .oneOf([true], "validate_privacy_agrrement_required")
        .required('validate_privacy_agrrement_required'),
    comment_box: yup.string()
        .required('dash_survey_validate_comment_required'),
    radiobutton: yup.string()
        .required('dash_survey_question_answer'),
    dropdown: yup.string()
        .required('dash_survey_question_answer'),
    rating: yup.boolean()
        .required('dash_survey_question_answer'),
    star_rating: yup.boolean()
        .required('dash_survey_question_answer'),
    text: yup.string()
        .required('dash_survey_question_answer'),
    datetime: yup.string()
        .required('dash_survey_question_answer'),
    slider: yup.string()
        .required('dash_survey_question_answer'),
    checkbox: yup.lazy(val => (Array.isArray(val) ? yup.array().of(yup.string().required('Your answer is required for this question')) : yup.string().required('Your answer is required for this question'))),
    description: yup.string()
        .required('dash_survey_description'),
    surveyName: yup.string()
        .required('dash_survey_name'),
    questionTitle: yup.string()
        .required('Title is required'),
    options: yup.string()
        .required('Options required'),
    language: yup.string()
        .required('dash_survey_language'),
    transactionMode: yup.string()
        .required('dash_survey_transactionMode'),
})
export const getValidationFields = (fields: string[]) => {
    return validationSchame.pick(fields as ("name" | "email")[]);
}

export interface FormikType {
    name?: string;
    value?: string;
    onchange: (e: React.ChangeEvent<any>) => void;
    error?: boolean;
    helperText?: string;
}

export default validationSchame;