const environments = {
  "localhost": {
    "api_auth_url": "https://survey-dev-com.herokuapp.com/auth",
    "api_surveys_url": "https://survey-dev-com.herokuapp.com/surveys",
  }
}
export default environments;