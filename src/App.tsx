import { Suspense } from 'react';
import {
  BrowserRouter as Router,
  Switch,
} from 'react-router-dom';
import routes from './config/routes';
import { AuthProvider } from './context';
import AppRoute from './components/common/AppRoutes';
import AppSuspenseLoading from './components/common/AppSuspenseLoading';

const App = () => {
  return (
    <Router>
      <Suspense fallback={<AppSuspenseLoading />}>
        <AuthProvider>
          <Switch>
            {routes.map((route) => (
              <AppRoute
                key={route.path}
                exact={route.exact}
                path={route.path}
                isPrivate={route.isPrivate}
                component={route.component}
              />
            ))}
          </Switch>
        </AuthProvider>
      </Suspense>
    </Router>
  );
}

export default App;